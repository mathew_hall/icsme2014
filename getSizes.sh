#!/usr/bin/env bash

MYDIR=$(dirname $0)

FILTER=0
if [ -n "$1" ]; then
	FILTER=$1
fi


for silfile in *.sil; do
	$MYDIR/getSize.pl $silfile $FILTER
done