# About

This repository contains experiment files from the ICSME 2014 ERA paper "Establishing the Source Code Disruption Caused by Automated Remodularisation Tools" by M Hall, MA Khojaye, N Walkinshaw, P McMinn.

# Contents

This repository contains the scripts used for each of the case studies we examined in our experiments. It also contains the data generated. See the `intialAnalysis.Rmd` for details on how the data were processed.

# Vagrant VM

The experiments were run on a virtual machine. The vagrantfile for this repository will not set up the Eclipse and source control environments on the VM, however. There is a separate VM image available that contains the state of the VM as we used it. It is bundled in an OVA appliance for import into VirtualBox in the `ICSME SUMO VM.ova` file.

# Running Experiments

Within the VM, the case study's files are kept within the Documents directory for the `vagrant` user.  The `launch_eclipse.sh` script will Eclipse, and hit the Refactor button on the SUMO plugin (preinstalled on the VM) to run a refactoring operation on a given SIL file.

The `run_case_studies.sh` file manages the support scripts. The paths for the case studies are hard-coded in this script, so they'll need to be changed for each case study. Additionally, the `case_study.sh` file also contains hard-coded names, so ensure both files are changed to use a different case study.

**All the scripts to run experiments need to be run from *within*  the VM to work, and must be run from a GUI so the X11 automation works.**

The VM needs configuring so that this repository is visible to the VM in the `/vagrant` directory as a shared folder. The username and password for the account is `vagrant`.

# Setting up Case Studies

The Eclipse install contains the plugin but needs to be pointed at a pre-created workspace for a case study. This is a manual step; you'll need to import the source code for the case study in a new workspace for each case study. Note that the scripts *rely* on UI elements not moving, so only add one project to the workspace.

# VM Note

The VM window can be resized, but this will interfere with the script. If you change the VM size, then the scripts will need to be changed accordingly to hit the right co-ordinates.