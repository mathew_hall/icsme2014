#!/usr/bin/env bash


echo "Setup: reverting pristine repo"

#FOR JEDIT:
#cd /home/vagrant/Documents/jedit-pristine/jedit-repo/jEdit/trunk

#FOR H2:
#cd /home/vagrant/Documents/h2db-pristine/h2db-svn/trunk/h2/src/main

#FOR vuze:
#cd /home/vagrant/Documents/vuze-pristine/vuze-svn/trunk/azureus2/src

#FOR JUnit:
#cd /home/vagrant/Documents/junit-pristine/junit-svn/junit/src/main/java

#FOR findbugs:
#cd /home/vagrant/Documents/findbugs-pristine/findbugs-svn/findbugs/findbugs/src/java

#FOR ant:
cd /home/vagrant/Documents/ant-pristine/ant-svn/ant/src/main

svn revert -R .
rm -r clusterLvl*

echo "Done reverting"

echo "Clearing eclipse src directory"

#FOR JEDIT:
#cd /home/vagrant/jedit/jedit
#rm -r src/*

#FOR H2:
#cd /home/vagrant/h2db/h2db
#rm -r src/*


echo "Done"