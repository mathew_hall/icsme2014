


# SUMO Commit sizes for diff_files_junit


```r
require(ggplot2)
```

```
## Loading required package: ggplot2
## Loading required package: methods
```

```r
require(plyr)
```

```
## Loading required package: plyr
```




```r
sizes <- read.csv(paste(path, "sizes_filter.csv", sep = "/"), header = F, stringsAsFactors = F)

names(sizes) <- c("sil_file", "file", "original_hunk_size", "new_hunk_size", 
    "additions", "deletions")



package <- function(string) {
    sapply(strsplit(string, "/"), function(x) x[1])
}

class <- function(string) {
    sapply(strsplit(string, "/"), function(x) x[2])
}

sizes$package <- package(sizes$file)
sizes$class <- class(sizes$file)


# Sanity checks: make sure that the sum of additions and deletions is
# consistent with hunk sizes:
netChanges <- sizes$additions - sizes$deletions
hunkChanges <- sizes$new_hunk_size - sizes$original_hunk_size

differences <- any((netChanges - hunkChanges) != 0)

# any differences?
differences
```

```
## [1] TRUE
```

```r

if (differences) {
    print("Error: some net changes don't correspond to hunk size changes")
}
```

```
## [1] "Error: some net changes don't correspond to hunk size changes"
```


## Distributions of additions and deletions for each commit hunk

A diff is divided into "hunks", contiguous changes in a file. A file might have more than one hunk.
This histogram shows the distribution of additions and deletions in each hunk in green and red respectively.

Each separate plot represents a single Bunch result.


```r
d_ply(sizes, .(sil_file), function(df) {
    sil_file <- df$sil_file[1]
    # ggplot(df, aes(file,additions+deletions, fill=file)) +
    # geom_bar(stat='identity');
    
    print(ggplot(df, aes(additions)) + geom_histogram(fill = "green", alpha = 0.4, 
        binwidth = 5) + geom_histogram(aes(deletions), fill = "red", alpha = 0.4, 
        binwidth = 5) + labs(x = "Lines") + theme_minimal())
    # ggsave(paste0(sil_file,'.png'));
})
```

![plot of chunk unnamed-chunk-4](figure/unnamed-chunk-41.png) ![plot of chunk unnamed-chunk-4](figure/unnamed-chunk-42.png) ![plot of chunk unnamed-chunk-4](figure/unnamed-chunk-43.png) ![plot of chunk unnamed-chunk-4](figure/unnamed-chunk-44.png) ![plot of chunk unnamed-chunk-4](figure/unnamed-chunk-45.png) ![plot of chunk unnamed-chunk-4](figure/unnamed-chunk-46.png) ![plot of chunk unnamed-chunk-4](figure/unnamed-chunk-47.png) ![plot of chunk unnamed-chunk-4](figure/unnamed-chunk-48.png) ![plot of chunk unnamed-chunk-4](figure/unnamed-chunk-49.png) ![plot of chunk unnamed-chunk-4](figure/unnamed-chunk-410.png) ![plot of chunk unnamed-chunk-4](figure/unnamed-chunk-411.png) ![plot of chunk unnamed-chunk-4](figure/unnamed-chunk-412.png) ![plot of chunk unnamed-chunk-4](figure/unnamed-chunk-413.png) ![plot of chunk unnamed-chunk-4](figure/unnamed-chunk-414.png) ![plot of chunk unnamed-chunk-4](figure/unnamed-chunk-415.png) ![plot of chunk unnamed-chunk-4](figure/unnamed-chunk-416.png) ![plot of chunk unnamed-chunk-4](figure/unnamed-chunk-417.png) ![plot of chunk unnamed-chunk-4](figure/unnamed-chunk-418.png) ![plot of chunk unnamed-chunk-4](figure/unnamed-chunk-419.png) ![plot of chunk unnamed-chunk-4](figure/unnamed-chunk-420.png) ![plot of chunk unnamed-chunk-4](figure/unnamed-chunk-421.png) ![plot of chunk unnamed-chunk-4](figure/unnamed-chunk-422.png) ![plot of chunk unnamed-chunk-4](figure/unnamed-chunk-423.png) ![plot of chunk unnamed-chunk-4](figure/unnamed-chunk-424.png) ![plot of chunk unnamed-chunk-4](figure/unnamed-chunk-425.png) ![plot of chunk unnamed-chunk-4](figure/unnamed-chunk-426.png) ![plot of chunk unnamed-chunk-4](figure/unnamed-chunk-427.png) ![plot of chunk unnamed-chunk-4](figure/unnamed-chunk-428.png) ![plot of chunk unnamed-chunk-4](figure/unnamed-chunk-429.png) ![plot of chunk unnamed-chunk-4](figure/unnamed-chunk-430.png) 


Summary of commit details for each Bunch run


```r
total_sizes <- ddply(sizes, .(sil_file), summarise, total_additions = sum(additions), 
    total_deletions = sum(deletions))
```


|sil_file  |  total_additions|  total_deletions|
|:---------|----------------:|----------------:|
|0.sil     |              812|              751|
|1.sil     |              824|              749|
|10.sil    |              779|              752|
|11.sil    |              787|              743|
|12.sil    |              799|              748|
|13.sil    |              801|              749|
|14.sil    |              806|              742|
|15.sil    |              835|              750|
|16.sil    |              801|              753|
|17.sil    |              805|              750|
|18.sil    |              794|              746|
|19.sil    |              780|              752|
|2.sil     |              790|              749|
|20.sil    |              784|              750|
|21.sil    |              830|              752|
|22.sil    |              790|              746|
|23.sil    |              811|              746|
|24.sil    |              803|              752|
|25.sil    |              819|              749|
|26.sil    |              806|              752|
|27.sil    |              799|              751|
|28.sil    |              816|              749|
|29.sil    |              802|              748|
|3.sil     |              793|              752|
|4.sil     |              808|              752|
|5.sil     |              801|              749|
|6.sil     |              820|              749|
|7.sil     |              803|              749|
|8.sil     |              800|              752|
|9.sil     |              782|              752|


## Distribution of total lines added/deleted in total


```r
ggplot(total_sizes, aes(total_additions)) + geom_histogram(fill = "green", alpha = 0.4, 
    binwidth = 50) + geom_histogram(aes(total_deletions), fill = "red", alpha = 0.4, 
    binwidth = 50) + labs(x = "Lines") + theme_minimal()
```

![plot of chunk unnamed-chunk-7](figure/unnamed-chunk-7.png) 


It's quite clear that there are more additions than deletions, but the spread for deletions is much tighter. If we look at each point individually:


```r

ggplot(total_sizes, aes(total_additions, total_deletions)) + geom_point() + 
    labs(x = "Lines added", y = "Lines removed") + theme_minimal()
```

![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-8.png) 


There are clear groups. The largest separation is between two populations with different numbers of lines removed and another with lines added.

This could correspond to different solutions Bunch produced, with the large separation occurring between two clearly different results. The smaller, within-group variation could be slight changes made by Bunch, i.e. points clustered around a local optimum.

## Averages:


```r
summary(total_sizes)
```

```
##    sil_file         total_additions total_deletions
##  Length:30          Min.   :779     Min.   :742    
##  Class :character   1st Qu.:793     1st Qu.:749    
##  Mode  :character   Median :802     Median :750    
##                     Mean   :803     Mean   :750    
##                     3rd Qu.:810     3rd Qu.:752    
##                     Max.   :835     Max.   :753
```


## How many hunks were only edits?


```r
hunkChanges <- ddply(sizes, .(sil_file), function(df) {
    df$changes <- df$additions - df$deletions
    df$netZero <- "NoChange"
    df$netZero[df$changes < 0] <- "Decrease"
    df$netZero[df$changes > 0] <- "Inrease"
    
    # print(ggplot(df, aes(changes)) + geom_histogram() + theme_minimal())
    print(ggplot(df, aes(netZero)) + geom_histogram() + theme_minimal() + labs(x = "Hunk did not change number of lines"))
    return(df)
})
```

![plot of chunk unnamed-chunk-10](figure/unnamed-chunk-101.png) ![plot of chunk unnamed-chunk-10](figure/unnamed-chunk-102.png) ![plot of chunk unnamed-chunk-10](figure/unnamed-chunk-103.png) ![plot of chunk unnamed-chunk-10](figure/unnamed-chunk-104.png) ![plot of chunk unnamed-chunk-10](figure/unnamed-chunk-105.png) ![plot of chunk unnamed-chunk-10](figure/unnamed-chunk-106.png) ![plot of chunk unnamed-chunk-10](figure/unnamed-chunk-107.png) ![plot of chunk unnamed-chunk-10](figure/unnamed-chunk-108.png) ![plot of chunk unnamed-chunk-10](figure/unnamed-chunk-109.png) ![plot of chunk unnamed-chunk-10](figure/unnamed-chunk-1010.png) ![plot of chunk unnamed-chunk-10](figure/unnamed-chunk-1011.png) ![plot of chunk unnamed-chunk-10](figure/unnamed-chunk-1012.png) ![plot of chunk unnamed-chunk-10](figure/unnamed-chunk-1013.png) ![plot of chunk unnamed-chunk-10](figure/unnamed-chunk-1014.png) ![plot of chunk unnamed-chunk-10](figure/unnamed-chunk-1015.png) ![plot of chunk unnamed-chunk-10](figure/unnamed-chunk-1016.png) ![plot of chunk unnamed-chunk-10](figure/unnamed-chunk-1017.png) ![plot of chunk unnamed-chunk-10](figure/unnamed-chunk-1018.png) ![plot of chunk unnamed-chunk-10](figure/unnamed-chunk-1019.png) ![plot of chunk unnamed-chunk-10](figure/unnamed-chunk-1020.png) ![plot of chunk unnamed-chunk-10](figure/unnamed-chunk-1021.png) ![plot of chunk unnamed-chunk-10](figure/unnamed-chunk-1022.png) ![plot of chunk unnamed-chunk-10](figure/unnamed-chunk-1023.png) ![plot of chunk unnamed-chunk-10](figure/unnamed-chunk-1024.png) ![plot of chunk unnamed-chunk-10](figure/unnamed-chunk-1025.png) ![plot of chunk unnamed-chunk-10](figure/unnamed-chunk-1026.png) ![plot of chunk unnamed-chunk-10](figure/unnamed-chunk-1027.png) ![plot of chunk unnamed-chunk-10](figure/unnamed-chunk-1028.png) ![plot of chunk unnamed-chunk-10](figure/unnamed-chunk-1029.png) ![plot of chunk unnamed-chunk-10](figure/unnamed-chunk-1030.png) 

```r

```


## Are net additions localised to certain files?


```r
largestChanges <- ddply(sizes, .(sil_file), function(sil) {
    perFile <- ddply(sil, .(sil_file, class), summarise, total_change = sum(additions - 
        deletions))
    perFile <- perFile[order(-perFile$total_change), ]
    perFile
})

averages <- ddply(largestChanges, .(class), summarise, average_change = mean(total_change))
averages <- averages[order(-averages$average_change), ]
kable(averages)
```

|id   |class                                             |  average_change|
|:----|:-------------------------------------------------|---------------:|
|126  |RunNotifier.java                                  |         3.00000|
|85   |JUnitCore.java                                    |         2.13333|
|117  |Rule.java                                         |         1.90000|
|32   |ClassRule.java                                    |         1.86667|
|105  |ParentRunner.java                                 |         1.70000|
|153  |TestRule.java                                     |         1.66667|
|11   |AnnotationValidatorFactory.java                   |         1.36667|
|22   |BlockJUnit4ClassRunner.java                       |         1.36667|
|115  |ResultMatchers.java                               |         1.16667|
|137  |Suite.java                                        |         1.10000|
|111  |ReflectiveCallable.java                           |         1.06667|
|122  |RunListener.java                                  |         1.00000|
|49   |ExpectedException.java                            |         0.96667|
|2    |After.java                                        |         0.83333|
|3    |AfterClass.java                                   |         0.83333|
|20   |Before.java                                       |         0.83333|
|41   |Description.java                                  |         0.83333|
|123  |Runner.java                                       |         0.80000|
|17   |AssumptionViolatedException1.java                 |         0.66667|
|28   |CategoryFilterFactory.java                        |         0.66667|
|83   |JUnit4TestCaseFacade.java                         |         0.66667|
|143  |TestCase.java                                     |         0.66667|
|161  |TextListener.java                                 |         0.66667|
|71   |IncludeCategories.java                            |         0.62069|
|6    |AllTests.java                                     |         0.60714|
|39   |DataPoints.java                                   |         0.60000|
|62   |FilterRequest.java                                |         0.60000|
|120  |RunAfters.java                                    |         0.60000|
|119  |RuleMemberValidator.java                          |         0.56667|
|124  |RunnerBuilder.java                                |         0.56667|
|38   |DataPoint.java                                    |         0.53333|
|88   |MaxCore.java                                      |         0.53333|
|58   |Filterable.java                                   |         0.50000|
|31   |ClassRequest.java                                 |         0.46667|
|36   |Computer.java                                     |         0.46667|
|107  |PrintableResult.java                              |         0.46667|
|110  |RealSystem.java                                   |         0.46667|
|114  |Result.java                                       |         0.46667|
|130  |Sorter.java                                       |         0.46667|
|154  |TestRunner.java                                   |         0.46667|
|65   |FrameworkMember.java                              |         0.43333|
|84   |JUnitCommandLineParseResult.java                  |         0.43333|
|87   |JUnitSystem.java                                  |         0.43333|
|170  |ValidationError.java                              |         0.40000|
|10   |AnnotationValidator.java                          |         0.36667|
|91   |MethodSorter.java                                 |         0.36667|
|108  |Protectable.java                                  |         0.36667|
|7    |Annotatable.java                                  |         0.33333|
|69   |IgnoredBuilder.java                               |         0.33333|
|139  |SuiteMethodBuilder.java                           |         0.30000|
|158  |TestWatcher.java                                  |         0.30000|
|24   |BlockJUnit4ClassRunnerWithParametersFactory.java  |         0.26667|
|29   |CategoryValidator.java                            |         0.26667|
|30   |Classes.java                                      |         0.26667|
|42   |EachTestNotifier.java                             |         0.26667|
|43   |Enclosed.java                                     |         0.26667|
|48   |ExcludeCategories.java                            |         0.26667|
|67   |FromDataPoints.java                               |         0.26667|
|109  |PublicClassValidator.java                         |         0.26667|
|135  |StoppedByUserException.java                       |         0.26667|
|147  |TestedOn.java                                     |         0.26667|
|148  |TestedOnSupplier.java                             |         0.26667|
|163  |Theory.java                                       |         0.26667|
|9    |AnnotationsValidator.java                         |         0.23333|
|66   |FrameworkMethod.java                              |         0.23333|
|75   |InvokeMethod.java                                 |         0.23333|
|81   |JUnit4TestAdapter.java                            |         0.23333|
|100  |ParameterizedAssertionError.java                  |         0.23333|
|103  |ParametersSuppliedBy.java                         |         0.23333|
|121  |RunBefores.java                                   |         0.23333|
|169  |ValidateWith.java                                 |         0.23333|
|82   |JUnit4TestAdapterCache.java                       |         0.20690|
|18   |AssumptionViolatedException2.java                 |         0.20000|
|26   |Categories.java                                   |         0.20000|
|59   |FilterFactories.java                              |         0.20000|
|68   |Ignore.java                                       |         0.20000|
|73   |InitializationError3.java                         |         0.20000|
|76   |junit                                             |         0.20000|
|80   |JUnit4Builder.java                                |         0.20000|
|89   |MaxHistory.java                                   |         0.20000|
|131  |SortingRequest.java                               |         0.20000|
|142  |Test.java                                         |         0.20000|
|159  |TestWatchman.java                                 |         0.20000|
|160  |TestWithParameters.java                           |         0.20000|
|45   |ErrorCollector.java                               |         0.17241|
|134  |Statement.java                                    |         0.17241|
|21   |BeforeClass.java                                  |         0.16667|
|40   |Describable.java                                  |         0.16667|
|61   |FilterFactoryParams.java                          |         0.16667|
|63   |FixMethodOrder.java                               |         0.16667|
|140  |SynchronizedRunListener.java                      |         0.16667|
|165  |ThrowableMessageMatcher.java                      |         0.16667|
|23   |BlockJUnit4ClassRunnerWithParameters.java         |         0.13333|
|46   |ErrorReportingRunner.java                         |         0.13333|
|51   |ExpectException.java                              |         0.13333|
|60   |FilterFactory.java                                |         0.13333|
|70   |IgnoredClassRunner.java                           |         0.13333|
|74   |InitializationError5.java                         |         0.13333|
|92   |MethodSorters.java                                |         0.13333|
|132  |SpecificDataPointsSupplier.java                   |         0.13333|
|95   |NoGenericTypeParametersValidator.java             |         0.10345|
|4    |AllDefaultPossibilitiesBuilder.java               |         0.10000|
|55   |Failure.java                                      |         0.10000|
|56   |FailureList.java                                  |         0.10000|
|96   |NoTestsRemainException.java                       |         0.10000|
|118  |RuleChain.java                                    |         0.10000|
|125  |RunnerScheduler.java                              |         0.10000|
|138  |SuiteMethod.java                                  |         0.10000|
|151  |TestName.java                                     |         0.10000|
|157  |TestTimedOutException.java                        |         0.10000|
|72   |InexactComparisonCriteria.java                    |         0.07407|
|16   |Assume.java                                       |         0.06667|
|19   |BaseTestRunner.java                               |         0.06667|
|37   |CouldNotReadCoreException.java                    |         0.06667|
|57   |Filter.java                                       |         0.06667|
|77   |JUnit38ClassRunner.java                           |         0.06667|
|104  |ParameterSupplier.java                            |         0.06667|
|129  |Sortable.java                                     |         0.06667|
|156  |TestSuite.java                                    |         0.06667|
|162  |Theories.java                                     |         0.06667|
|53   |Fail.java                                         |         0.03448|
|141  |TemporaryFolder.java                              |         0.03448|
|8    |AnnotatedBuilder.java                             |         0.03333|
|15   |Assignments.java                                  |         0.03333|
|25   |BooleanSupplier.java                              |         0.03333|
|27   |Category.java                                     |         0.03333|
|33   |ComparisonCompactor.java                          |         0.03333|
|34   |ComparisonCriteria.java                           |         0.03333|
|64   |FrameworkField.java                               |         0.03333|
|94   |MultipleFailureException6.java                    |         0.03333|
|97   |NullBuilder.java                                  |         0.03333|
|101  |ParameterSignature.java                           |         0.03333|
|106  |PotentialAssignment.java                          |         0.03333|
|112  |RepeatedTest.java                                 |         0.03333|
|113  |Request.java                                      |         0.03333|
|128  |RunWith.java                                      |         0.03333|
|145  |TestClassValidator.java                           |         0.03333|
|146  |TestDecorator.java                                |         0.03333|
|155  |TestSetup.java                                    |         0.03333|
|168  |TypeSafeMatcher.java                              |         0.03333|
|172  |Version.java                                      |         0.03333|
|1    |ActiveTestSuite.java                              |         0.00000|
|5    |AllMembersSupplier.java                           |         0.00000|
|12   |ArrayComparisonFailure.java                       |         0.00000|
|13   |Assert.java                                       |         0.00000|
|14   |AssertionFailedError.java                         |         0.00000|
|35   |ComparisonFailure.java                            |         0.00000|
|44   |EnumSupplier.java                                 |         0.00000|
|47   |ExactComparisonCriteria.java                      |         0.00000|
|50   |ExpectedExceptionMatcherBuilder.java              |         0.00000|
|52   |ExternalResource.java                             |         0.00000|
|54   |FailOnTimeout.java                                |         0.00000|
|78   |JUnit3Builder.java                                |         0.00000|
|79   |JUnit4.java                                       |         0.00000|
|86   |JUnitMatchers.java                                |         0.00000|
|90   |MethodRule.java                                   |         0.00000|
|93   |MultipleFailureException4.java                    |         0.00000|
|98   |ParallelComputer.java                             |         0.00000|
|99   |Parameterized.java                                |         0.00000|
|102  |ParametersRunnerFactory.java                      |         0.00000|
|116  |ResultPrinter.java                                |         0.00000|
|127  |RunRules.java                                     |         0.00000|
|133  |StacktracePrintingMatcher.java                    |         0.00000|
|136  |Stopwatch.java                                    |         0.00000|
|144  |TestClass.java                                    |         0.00000|
|149  |TestFailure.java                                  |         0.00000|
|150  |TestListener.java                                 |         0.00000|
|152  |TestResult.java                                   |         0.00000|
|164  |ThrowableCauseMatcher.java                        |         0.00000|
|166  |Throwables.java                                   |         0.00000|
|167  |Timeout.java                                      |         0.00000|
|171  |Verifier.java                                     |         0.00000|

