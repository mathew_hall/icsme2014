#!/usr/bin/env bash 

#Path to an Eclipse workspace to load, with the project already open:

###H2
#export WORKSPACE="/home/vagrant/h2db"
###jEdit
#export WORKSPACE="/home/vagrant/jedit"
###Vuze
#export WORKSPACE="/home/vagrant/vuze"
###JUnit
#export WORKSPACE="/home/vagrant/junit"
###findbugs
#export WORKSPACE="/home/vagrant/findbugs"
###ant
export WORKSPACE="/home/vagrant/ant"

#Path to the code's SVN repo, checked out. Files will be moved here after renames:

###H2
#export WORKING_COPY="/home/vagrant/Documents/h2db-pristine/h2db-svn/trunk/h2/src/main"
###jEdit
#export WORKING_COPY="/home/vagrant/Documents/jedit-pristine/jedit-repo/jEdit/trunk"
###vuze
#export WORKING_COPY="/home/vagrant/Documents/vuze-pristine/vuze-svn/trunk/azureus2/src"
###JUnit
#export WORKING_COPY="/home/vagrant/Documents/junit-pristine/junit-svn/junit/src/main/java"
###findbugs
#export WORKING_COPY="/home/vagrant/Documents/findbugs-pristine/findbugs-svn/findbugs/findbugs/src/java"
###ant
export WORKING_COPY="/home/vagrant/Documents/ant-pristine/ant-svn/ant/src/main"

#Path where files will get moved to (should normally be same as working copy):

###H2
#export NEW_SRC_DIR="$WORKING_COPY"
###jEdit
#export NEW_SRC_DIR="$WORKSPACE/jedit/src"
###vuze
#export NEW_SRC_DIR="$WORKING_COPY"
###JUnit
#export NEW_SRC_DIR="$WORKING_COPY"
###findbugs
#export NEW_SRC_DIR="$WORKING_COPY"
###Ant
export NEW_SRC_DIR="$WORKING_COPY"

echo "[*] Reverting repo and restoring workspace state."
/vagrant/case_study.sh

for silfile in /vagrant/sil_files/*.sil; do


echo "[*] Applying refactoring for $silfile"
cd /home/vagrant/Downloads/eclipse
/vagrant/launch_eclipse.sh $silfile

echo "[*] Fixing up project"
cd $WORKING_COPY

#echo "Debug started! Check the working copy now!"

#debugging:
#exit;

#No idea why running both at the same time breaks it, but it does, so
#first move the files to their original places
/vagrant/fix_renames.pl $silfile $(pwd) $NEW_SRC_DIR 1 0
#Then rename them
/vagrant/fix_renames.pl $silfile $(pwd) $NEW_SRC_DIR 0 1
#debugging:
#exit

outdir=/vagrant/diff_files/$(basename $silfile)
echo "[*] Getting diff to $outdir"
svn diff > $outdir

echo "[*] Reverting repo and restoring workspace state."
/vagrant/case_study.sh


done

