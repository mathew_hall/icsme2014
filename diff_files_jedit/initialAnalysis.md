# SUMO Commit sizes for jEdit


```r

require(ggplot2)
```

```
## Loading required package: ggplot2
## Loading required package: methods
```

```r
require(plyr)
```

```
## Loading required package: plyr
```

```r

sizes <- read.csv("sizes_filter.csv", header = F, stringsAsFactors = F)

names(sizes) <- c("sil_file", "file", "original_hunk_size", "new_hunk_size", 
    "additions", "deletions")



package <- function(string) {
    sapply(strsplit(string, "/"), function(x) x[1])
}

class <- function(string) {
    sapply(strsplit(string, "/"), function(x) x[2])
}

sizes$package <- package(sizes$file)
sizes$class <- class(sizes$file)


# Sanity checks: make sure that the sum of additions and deletions is
# consistent with hunk sizes:
netChanges <- sizes$additions - sizes$deletions
hunkChanges <- sizes$new_hunk_size - sizes$original_hunk_size

differences <- any((netChanges - hunkChanges) != 0)

# any differences?
differences
```

```
## [1] TRUE
```

```r

if (differences) {
    print("Error: some net changes don't correspond to hunk size changes")
}
```

```
## [1] "Error: some net changes don't correspond to hunk size changes"
```


## Distributions of additions and deletions for each commit hunk

A diff is divided into "hunks", contiguous changes in a file. A file might have more than one hunk.
This histogram shows the distribution of additions and deletions in each hunk in green and red respectively.

Each separate plot represents a single Bunch result.


```r
d_ply(sizes, .(sil_file), function(df) {
    sil_file <- df$sil_file[1]
    # ggplot(df, aes(file,additions+deletions, fill=file)) +
    # geom_bar(stat='identity');
    
    print(ggplot(df, aes(additions)) + geom_histogram(fill = "green", alpha = 0.4, 
        binwidth = 5) + geom_histogram(aes(deletions), fill = "red", alpha = 0.4, 
        binwidth = 5) + labs(x = "Lines") + theme_minimal())
    # ggsave(paste0(sil_file,'.png'));
})
```

![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-21.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-22.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-23.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-24.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-25.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-26.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-27.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-28.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-29.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-210.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-211.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-212.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-213.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-214.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-215.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-216.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-217.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-218.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-219.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-220.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-221.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-222.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-223.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-224.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-225.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-226.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-227.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-228.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-229.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-230.png) 


Summary of commit details for each Bunch run


```r
total_sizes <- ddply(sizes, .(sil_file), summarise, total_additions = sum(additions), 
    total_deletions = sum(deletions))
```


|sil_file  |  total_additions|  total_deletions|
|:---------|----------------:|----------------:|
|0.sil     |             3051|             1816|
|1.sil     |             2811|             1815|
|10.sil    |             3074|             1817|
|11.sil    |             3032|             1812|
|12.sil    |             3051|             1815|
|13.sil    |             3074|             1991|
|14.sil    |             3049|             1990|
|15.sil    |             2995|             1807|
|16.sil    |             2524|             1992|
|17.sil    |             2837|             1814|
|18.sil    |             3023|             1811|
|19.sil    |             3087|             1809|
|2.sil     |             2538|             1813|
|20.sil    |             2950|             1992|
|21.sil    |             2781|             1813|
|22.sil    |             3067|             1805|
|23.sil    |             3050|             1811|
|24.sil    |             3128|             1812|
|25.sil    |             3075|             1817|
|26.sil    |             3053|             1815|
|27.sil    |             3013|             1814|
|28.sil    |             3081|             1817|
|29.sil    |             2962|             1809|
|3.sil     |             2934|             1994|
|4.sil     |             2783|             1813|
|5.sil     |             2904|             1815|
|6.sil     |             3064|             1814|
|7.sil     |             3035|             1816|
|8.sil     |             2972|             1815|
|9.sil     |             3093|             1993|


## Distribution of total lines added/deleted in total


```r
ggplot(total_sizes, aes(total_additions)) + geom_histogram(fill = "green", alpha = 0.4, 
    binwidth = 50) + geom_histogram(aes(total_deletions), fill = "red", alpha = 0.4, 
    binwidth = 50) + labs(x = "Lines") + theme_minimal()
```

![plot of chunk unnamed-chunk-5](figure/unnamed-chunk-5.png) 


It's quite clear that there are more additions than deletions, but the spread for deletions is much tighter. If we look at each point individually:


```r

ggplot(total_sizes, aes(total_additions, total_deletions)) + geom_point() + 
    labs(x = "Lines added", y = "Lines removed") + theme_minimal()
```

![plot of chunk unnamed-chunk-6](figure/unnamed-chunk-6.png) 


There are clear groups. The largest separation is between two populations with different numbers of lines removed and another with lines added.

This could correspond to different solutions Bunch produced, with the large separation occurring between two clearly different results. The smaller, within-group variation could be slight changes made by Bunch, i.e. points clustered around a local optimum.

## Averages:


```r
summary(total_sizes)
```

```
##    sil_file         total_additions total_deletions
##  Length:30          Min.   :2524    Min.   :1805   
##  Class :character   1st Qu.:2938    1st Qu.:1812   
##  Mode  :character   Median :3034    Median :1815   
##                     Mean   :2970    Mean   :1849   
##                     3rd Qu.:3066    3rd Qu.:1817   
##                     Max.   :3128    Max.   :1994
```


## How many hunks were only edits?


```r
hunkChanges <- ddply(sizes, .(sil_file), function(df) {
    df$changes <- df$additions - df$deletions
    df$netZero <- "NoChange"
    df$netZero[df$changes < 0] <- "Decrease"
    df$netZero[df$changes > 0] <- "Inrease"
    
    # print(ggplot(df, aes(changes)) + geom_histogram() + theme_minimal())
    print(ggplot(df, aes(netZero)) + geom_histogram() + theme_minimal() + labs(x = "Hunk did not change number of lines"))
    return(df)
})
```

![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-81.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-82.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-83.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-84.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-85.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-86.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-87.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-88.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-89.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-810.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-811.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-812.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-813.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-814.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-815.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-816.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-817.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-818.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-819.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-820.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-821.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-822.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-823.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-824.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-825.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-826.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-827.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-828.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-829.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-830.png) 

```r

```


## Are net additions localised to certain files?


```r
largestChanges <- ddply(sizes, .(sil_file), function(sil) {
    perFile <- ddply(sil, .(sil_file, class), summarise, total_change = sum(additions - 
        deletions))
    perFile <- perFile[order(-perFile$total_change), ]
    perFile
})

averages <- ddply(largestChanges, .(class), summarise, average_change = mean(total_change))
averages <- averages[order(-averages$average_change), ]
kable(averages)
```

|id   |class                                  |  average_change|
|:----|:--------------------------------------|---------------:|
|283  |jEdit.java                             |        52.20000|
|135  |Constants.java                         |        13.66667|
|472  |Task.java                              |        12.03333|
|357  |NameSpace.java                         |        11.60000|
|66   |BSHSwitchLabel.java                    |        11.13333|
|274  |Interpreter.java                       |        10.90000|
|40   |BSHBinaryExpression.java               |         9.56667|
|521  |View.java                              |         9.30000|
|325  |ListModelEditor.java                   |         9.03333|
|490  |This.java                              |         8.53333|
|268  |InputHandler.java                      |         8.43333|
|271  |InstallPanel.java                      |         8.40000|
|288  |JEditBuffer.java                       |         8.13333|
|510  |VFS.java                               |         8.06667|
|491  |ThreadUtilities.java                   |         7.93333|
|187  |EditServer.java                        |         7.90000|
|478  |TextArea.java                          |         7.66667|
|21   |BeanShell.java                         |         7.63333|
|89   |BufferPrintable.java                   |         7.40000|
|286  |JEditActionSet.java                    |         7.06667|
|332  |ManagePanel.java                       |         7.00000|
|45   |BshClassManager.java                   |         6.63333|
|517  |VFSFileNameField.java                  |         6.53333|
|279  |Item.java                              |         6.40000|
|184  |EditPane.java                          |         6.20000|
|97   |BufferSetManager.java                  |         6.13333|
|530  |XThis.java                             |         6.06667|
|235  |GUIUtilities.java                      |         6.00000|
|319  |Label.java                             |         5.80000|
|384  |PluginList.java                        |         5.76667|
|76   |Buffer.java                            |         5.73333|
|47   |BSHEnhancedForStatement.java           |         5.60000|
|12   |ActionSet.java                         |         5.53333|
|518  |VFSManager.java                        |         5.43333|
|64   |BSHReturnType.java                     |         4.96667|
|186  |EditPlugin.java                        |         4.93333|
|148  |DelayedEvalBshMethod.java              |         4.73333|
|161  |DockableWindowManager.java             |         4.70000|
|359  |Node.java                              |         4.63333|
|324  |ListDirectoryBrowserTask.java          |         4.46667|
|49   |BSHFormalParameter.java                |         4.33333|
|56   |BshMethod.java                         |         4.33333|
|59   |BSHPackageDeclaration.java             |         4.33333|
|442  |SelectionManager.java                  |         4.20000|
|117  |ClassVisitor.java                      |         4.06667|
|424  |RolloverButton.java                    |         4.06667|
|118  |ClassWriter.java                       |         4.03448|
|210  |ExternalNameSpace.java                 |         4.00000|
|469  |SyntaxUtilities.java                   |         3.96667|
|507  |UtilTargetError.java                   |         3.96667|
|16   |AntiAlias.java                         |         3.90000|
|178  |EditAction.java                        |         3.90000|
|81   |BufferHistory.java                     |         3.86667|
|110  |ClassGenerator.java                    |         3.86667|
|152  |DirectoryProvider.java                 |         3.86667|
|508  |Variable.java                          |         3.86667|
|62   |BSHPrimitiveType.java                  |         3.80000|
|434  |SearchDialog.java                      |         3.76667|
|126  |CollectionManagerImpl.java             |         3.73333|
|405  |RecentFilesProvider.java               |         3.73333|
|99   |BufferSwitcher.java                    |         3.70000|
|348  |Modifiers.java                         |         3.70000|
|487  |TextAreaPainter.java                   |         3.70000|
|503  |Types.java                             |         3.70000|
|505  |UrlVFS.java                            |         3.70000|
|445  |ServiceManager.java                    |         3.66667|
|140  |CurrentBufferSet.java                  |         3.56667|
|177  |EditAbbrevDialog.java                  |         3.56667|
|258  |HyperSearchRequest.java                |         3.56667|
|338  |MenuItemTextComparator.java            |         3.56667|
|71   |BSHType.java                           |         3.53333|
|246  |HelpViewerInterface.java               |         3.53333|
|392  |PluginsProvider.java                   |         3.53333|
|20   |AwtRunnableQueue.java                  |         3.50000|
|50   |BSHFormalParameters.java               |         3.50000|
|65   |BSHStatementExpressionList.java        |         3.50000|
|121  |CloseDialog.java                       |         3.50000|
|304  |JJTParserState.java                    |         3.46667|
|344  |Mode.java                              |         3.46667|
|39   |BSHAssignment.java                     |         3.43333|
|113  |ClassIdentifier.java                   |         3.43333|
|493  |Token.java                             |         3.43333|
|122  |CodeVisitor.java                       |         3.40000|
|36   |BSHArguments.java                      |         3.36667|
|321  |LHS.java                               |         3.36667|
|388  |PluginManagerProgress.java             |         3.36667|
|67   |BSHSwitchStatement.java                |         3.30000|
|104  |Capabilities.java                      |         3.26667|
|336  |MarkerViewer.java                      |         3.24138|
|38   |BSHArrayInitializer.java               |         3.23333|
|155  |DisplayTokenHandler.java               |         3.20000|
|179  |EditBus.java                           |         3.20000|
|488  |TextAreaTransferHandler.java           |         3.10000|
|88   |BufferOptions.java                     |         3.06667|
|452  |SplashScreen.java                      |         3.06667|
|512  |VFSDirectoryEntryTable.java            |         3.06667|
|124  |CollectionIterator.java                |         3.03333|
|420  |ReturnControl.java                     |         3.03333|
|202  |EnhancedTreeCellRenderer.java          |         3.00000|
|216  |FileRootsVFS.java                      |         3.00000|
|261  |HyperSearchTreeNodeCallback.java       |         3.00000|
|275  |InterpreterError.java                  |         3.00000|
|51   |BSHForStatement.java                   |         2.96667|
|333  |Marker.java                            |         2.96667|
|376  |ParserTreeConstants.java               |         2.96552|
|394  |PositionChanging.java                  |         2.93333|
|496  |TokenMgrError.java                     |         2.93333|
|35   |BSHAmbiguousName.java                  |         2.90000|
|52   |BSHIfStatement.java                    |         2.90000|
|94   |BufferSet.java                         |         2.90000|
|139  |CopyFileWorker.java                    |         2.90000|
|157  |DockablePanel.java                     |         2.90000|
|229  |FontSelectorDialog.java                |         2.90000|
|248  |HistoryModel.java                      |         2.90000|
|410  |ReflectManagerImpl.java                |         2.90000|
|440  |Selection.java                         |         2.90000|
|48   |BSHFormalComment.java                  |         2.80000|
|213  |FileCellRenderer.java                  |         2.80000|
|331  |MacrosProvider.java                    |         2.80000|
|471  |TargetError.java                       |         2.80000|
|2    |Abbrevs.java                           |         2.76667|
|199  |EnhancedDialog.java                    |         2.76667|
|450  |ShortcutsOptionPane.java               |         2.76667|
|329  |LogViewer.java                         |         2.73333|
|464  |StringUtil.java                        |         2.73333|
|443  |SelectLineRange.java                   |         2.70000|
|1    |AbbrevEditor.java                      |         2.66667|
|37   |BSHArrayDimensions.java                |         2.66667|
|387  |PluginManagerOptionPane.java           |         2.66667|
|426  |SaveBackupOptionPane.java              |         2.66667|
|520  |VFSUpdate.java                         |         2.66667|
|18   |AutoDetection.java                     |         2.63333|
|150  |dir.java                               |         2.63333|
|46   |BshClassPath.java                      |         2.60000|
|498  |ToolBarOptionPane.java                 |         2.60000|
|251  |HistoryTextArea.java                   |         2.56667|
|326  |ListVFSFileTransferable.java           |         2.56667|
|513  |VFSDirectoryEntryTableModel.java       |         2.56667|
|58   |BSHMethodInvocation.java               |         2.53333|
|353  |MultiSelectWidgetFactory.java          |         2.53333|
|371  |Parser.java                            |         2.53333|
|5    |AbstractBrowserTask.java               |         2.50000|
|417  |RegisterViewer.java                    |         2.50000|
|479  |TextAreaBorder.java                    |         2.50000|
|526  |WrapWidgetFactory.java                 |         2.50000|
|60   |BSHPrimaryExpression.java              |         2.46667|
|408  |ReflectError.java                      |         2.46667|
|242  |HelpIndex.java                         |         2.44828|
|68   |BSHTernaryExpression.java              |         2.43333|
|114  |ClassManagerImpl.java                  |         2.43333|
|34   |BSHAllocationExpression.java           |         2.40000|
|151  |DirectoryListSet.java                  |         2.40000|
|366  |OptionPane.java                        |         2.40000|
|396  |Primitive.java                         |         2.40000|
|409  |ReflectManager.java                    |         2.40000|
|474  |TaskListener.java                      |         2.40000|
|61   |BSHPrimarySuffix.java                  |         2.36667|
|165  |DockingFrameworkProvider.java          |         2.36667|
|277  |IOUtilities.java                       |         2.36667|
|363  |OperatingSystem.java                   |         2.36667|
|4    |AboutDialog.java                       |         2.33333|
|63   |BSHReturnStatement.java                |         2.30000|
|285  |JEditActionContext.java                |         2.30000|
|41   |BSHBlock.java                          |         2.26667|
|72   |BSHTypedVariableDeclaration.java       |         2.26667|
|43   |BSHClassDeclaration.java               |         2.23333|
|138  |ContextOptionPane.java                 |         2.23333|
|309  |KeyEventTranslator.java                |         2.23333|
|330  |Macros.java                            |         2.23333|
|414  |Registers.java                         |         2.23333|
|421  |ReverseCharSequence.java               |         2.23333|
|494  |TokenHandler.java                      |         2.23333|
|506  |UtilEvalError.java                     |         2.23333|
|9    |ActionBar.java                         |         2.20000|
|57   |BSHMethodDeclaration.java              |         2.20000|
|171  |DynamicMenuChanged.java                |         2.20000|
|339  |MigrationService.java                  |         2.20000|
|166  |DockingLayoutManager.java              |         2.16667|
|23   |BeanShellErrorDialog.java              |         2.13333|
|221  |FirewallOptionPane.java                |         2.13333|
|228  |FontSelector.java                      |         2.13333|
|334  |MarkersProvider.java                   |         2.13333|
|389  |PluginOptionGroup.java                 |         2.13333|
|116  |ClassPathListener.java                 |         2.10000|
|273  |IntegerInputVerifier.java              |         2.10000|
|291  |JEditHistoryModelSaver.java            |         2.10000|
|305  |JThis.java                             |         2.10000|
|419  |RenameBrowserTask.java                 |         2.10000|
|393  |PluginUpdate.java                      |         2.06897|
|292  |JEditKillRing.java                     |         2.06667|
|383  |PluginJAR.java                         |         2.06667|
|484  |TextAreaInputHandler.java              |         2.06667|
|86   |BufferLoadRequest.java                 |         2.03333|
|232  |GlobalOptions.java                     |         2.03333|
|19   |Autosave.java                          |         2.00000|
|79   |BufferChanging.java                    |         2.00000|
|125  |CollectionManager.java                 |         2.00000|
|128  |ColumnBlock.java                       |         2.00000|
|137  |ContextAddDialog.java                  |         2.00000|
|33   |BrowserView.java                       |         1.96667|
|28   |BrowserColorsOptionPane.java           |         1.93333|
|70   |BSHTryStatement.java                   |         1.93333|
|111  |ClassGeneratorImpl.java                |         1.93333|
|197  |EnhancedButton.java                    |         1.93333|
|310  |KeyEventWorkaround.java                |         1.93333|
|358  |Native2ASCIIEncoding.java              |         1.93333|
|382  |PluginDetailPanel.java                 |         1.93333|
|473  |TaskAdapter.java                       |         1.93333|
|511  |VFSBrowser.java                        |         1.93333|
|143  |DefaultFocusComponent.java             |         1.90000|
|240  |HelpHistoryModel.java                  |         1.90000|
|407  |Reflect.java                           |         1.90000|
|212  |FavoritesVFS.java                      |         1.86667|
|346  |ModeProvider.java                      |         1.86667|
|385  |PluginListHandler.java                 |         1.86667|
|447  |SettingsXML.java                       |         1.86667|
|176  |Edge.java                              |         1.83333|
|352  |MouseOptionPane.java                   |         1.83333|
|369  |PanelWindowContainer.java              |         1.83333|
|14   |AllBufferSet.java                      |         1.76667|
|200  |EnhancedMenu.java                      |         1.76667|
|255  |HyperSearchFolderNode.java             |         1.76667|
|372  |ParserConstants.java                   |         1.76667|
|25   |BlockNameSpace.java                    |         1.73333|
|74   |BSHVariableDeclarator.java             |         1.73333|
|106  |CheckFileStatus.java                   |         1.73333|
|146  |DefaultInputHandlerProvider.java       |         1.73333|
|160  |DockableWindowFactory.java             |         1.73333|
|191  |Encoding.java                          |         1.73333|
|244  |HelpTOCPanel.java                      |         1.73333|
|308  |KeyboardCommand.java                   |         1.73333|
|141  |Debug.java                             |         1.70000|
|185  |EditPaneUpdate.java                    |         1.70000|
|208  |ExtendedGridLayoutConstraints.java     |         1.70000|
|30   |BrowserContextOptionPane.java          |         1.66667|
|172  |DynamicMenuProvider.java               |         1.66667|
|233  |GlobVFSFileFilter.java                 |         1.66667|
|303  |JEditVisitorAdapter.java               |         1.65517|
|53   |BSHImportDeclaration.java              |         1.63333|
|69   |BSHThrowStatement.java                 |         1.63333|
|84   |BufferListener.java                    |         1.63333|
|112  |ClassGeneratorUtil.java                |         1.63333|
|170  |DynamicContextMenuService.java         |         1.63333|
|307  |JTrayIconManager.java                  |         1.63333|
|340  |MirrorList.java                        |         1.63333|
|347  |ModeWidgetFactory.java                 |         1.63333|
|355  |Name.java                              |         1.63333|
|435  |SearchFileSet.java                     |         1.63333|
|515  |VFSFileChooserDialog.java              |         1.63333|
|516  |VFSFileFilter.java                     |         1.63333|
|174  |EBMessage.java                         |         1.62069|
|7    |AbstractInputHandler.java              |         1.60000|
|85   |BufferListSet.java                     |         1.60000|
|92   |BufferSaveRequest.java                 |         1.60000|
|127  |ColorWellButton.java                   |         1.60000|
|243  |HelpSearchPanel.java                   |         1.60000|
|42   |BSHCastExpression.java                 |         1.56667|
|250  |HistoryText.java                       |         1.56667|
|281  |JavaCharStream.java                    |         1.56667|
|337  |MemoryStatusWidgetFactory.java         |         1.56667|
|343  |MkDirBrowserTask.java                  |         1.56667|
|519  |VFSPathSelected.java                   |         1.56667|
|458  |StatusBarOptionPane.java               |         1.55172|
|198  |EnhancedCheckBoxMenuItem.java          |         1.53333|
|241  |HelpHistoryModelListener.java          |         1.53333|
|314  |KeymapManager.java                     |         1.53333|
|400  |PropertiesChanged.java                 |         1.53333|
|514  |VFSFile.java                           |         1.53333|
|3    |AbbrevsOptionPane.java                 |         1.50000|
|6    |AbstractContextOptionPane.java         |         1.50000|
|22   |BeanShellAction.java                   |         1.50000|
|214  |FileOpenerService.java                 |         1.50000|
|259  |HyperSearchResult.java                 |         1.50000|
|327  |LocalFileSaveTask.java                 |         1.50000|
|341  |MirrorListHandler.java                 |         1.50000|
|368  |OverwriteWidgetFactory.java            |         1.50000|
|26   |BoyerMooreSearchMatcher.java           |         1.48276|
|31   |BrowserListener.java                   |         1.46667|
|145  |DefaultInputHandler.java               |         1.46667|
|180  |EditingOptionPane.java                 |         1.46667|
|182  |EditorExitRequested.java               |         1.46667|
|523  |ViewUpdate.java                        |         1.46667|
|83   |BufferIORequest.java                   |         1.43333|
|418  |ReloadWithEncodingProvider.java        |         1.43333|
|455  |StandaloneTextArea.java                |         1.43333|
|73   |BSHUnaryExpression.java                |         1.40000|
|219  |FilteredListModel.java                 |         1.40000|
|294  |JEditRegisterSaver.java                |         1.40000|
|312  |KeymapFileFilter.java                  |         1.40000|
|401  |PropertiesChanging.java                |         1.40000|
|437  |SearchSettingsChanged.java             |         1.40000|
|463  |StringTransferableService.java         |         1.37931|
|8    |AbstractOptionPane.java                |         1.36667|
|32   |BrowserOptionPane.java                 |         1.36667|
|207  |ExtendedGridLayout.java                |         1.36667|
|276  |IoTask.java                            |         1.36667|
|451  |SimpleNode.java                        |         1.36667|
|456  |StandardUtilities.java                 |         1.36667|
|457  |StatusBar.java                         |         1.36667|
|217  |FilesChangedDialog.java                |         1.33333|
|224  |FoldHandler.java                       |         1.33333|
|260  |HyperSearchResults.java                |         1.33333|
|374  |ParserRuleSet.java                     |         1.33333|
|446  |SettingsReloader.java                  |         1.33333|
|460  |StatusWidgetFactory.java               |         1.33333|
|468  |SyntaxStyle.java                       |         1.33333|
|82   |BufferInsertRequest.java               |         1.30000|
|239  |Handler.java                           |         1.30000|
|313  |KeymapImpl.java                        |         1.30000|
|322  |LineManager.java                       |         1.30000|
|350  |MouseActionsProvider.java              |         1.30000|
|404  |RecentDirectoriesProvider.java         |         1.30000|
|425  |Roster.java                            |         1.30000|
|29   |BrowserCommandsMenu.java               |         1.27586|
|222  |FirstLine.java                         |         1.27586|
|354  |MutableListModel.java                  |         1.27586|
|130  |CombinedOptions.java                   |         1.26667|
|175  |EBPlugin.java                          |         1.26667|
|315  |KeymapManagerImpl.java                 |         1.26667|
|361  |OneTimeMigrationService.java           |         1.26667|
|364  |OptionGroup.java                       |         1.26667|
|432  |SearchAndReplace.java                  |         1.26667|
|453  |SplitConfigParser.java                 |         1.26667|
|54   |BshIterator.java                       |         1.23333|
|149  |DeleteBrowserTask.java                 |         1.23333|
|194  |EncodingsOptionPane.java               |         1.23333|
|223  |FloatingWindowContainer.java           |         1.23333|
|378  |PasteSpecialDialog.java                |         1.23333|
|380  |PerspectiveManager.java                |         1.23333|
|465  |StructureMatcher.java                  |         1.23333|
|10   |ActionContext.java                     |         1.20000|
|78   |BufferAutosaveRequest.java             |         1.20000|
|134  |ConsoleInterface.java                  |         1.20000|
|183  |EditorStarted.java                     |         1.20000|
|278  |IPropertyManager.java                  |         1.20000|
|282  |JCheckBoxList.java                     |         1.20000|
|528  |XMLUtilities.java                      |         1.20000|
|15   |Anchor.java                            |         1.16667|
|91   |BufferPrinter1_4.java                  |         1.16667|
|195  |EncodingWidgetFactory.java             |         1.16667|
|263  |IndentAction.java                      |         1.16667|
|267  |IndentWidgetFactory.java               |         1.16667|
|397  |PrintOptionPane.java                   |         1.13793|
|55   |BSHLiteral.java                        |         1.13333|
|96   |BufferSetListener.java                 |         1.13333|
|98   |BufferSetWidgetFactory.java            |         1.13333|
|253  |HtmlUtilities.java                     |         1.13333|
|298  |JEditTextArea.java                     |         1.13333|
|379  |PatternSearchMatcher.java              |         1.13333|
|158  |DockableWindow.java                    |         1.10000|
|211  |FavoritesProvider.java                 |         1.10000|
|226  |FoldPainter.java                       |         1.10000|
|237  |GutterOptionPane.java                  |         1.10000|
|296  |JEditRichText.java                     |         1.10000|
|373  |ParserRule.java                        |         1.10000|
|398  |ProgressObserver.java                  |         1.10000|
|411  |RegexEncodingDetector.java             |         1.10000|
|449  |ShortcutPrefixActiveEvent.java         |         1.10000|
|477  |TaskMonitorWidgetFactory.java          |         1.10000|
|492  |TipOfTheDay.java                       |         1.10000|
|320  |LastModifiedWidgetFactory.java         |         1.06897|
|205  |EvalError.java                         |         1.06667|
|245  |HelpViewer.java                        |         1.06667|
|249  |HistoryModelSaver.java                 |         1.06667|
|335  |MarkersSaveRequest.java                |         1.06667|
|345  |ModeCatalogHandler.java                |         1.06667|
|367  |OptionsDialog.java                     |         1.06667|
|375  |ParserTokenManager.java                |         1.06667|
|406  |RectSelectWidgetFactory.java           |         1.06667|
|433  |SearchBar.java                         |         1.06667|
|467  |SyntaxHiliteOptionPane.java            |         1.06667|
|480  |TextAreaDialog.java                    |         1.06667|
|13   |AddAbbrevDialog.java                   |         1.03333|
|103  |CallStack.java                         |         1.03333|
|153  |DiscreteFilesClassLoader.java          |         1.03333|
|206  |ExplicitFoldHandler.java               |         1.03333|
|215  |FilePropertiesDialog.java              |         1.03333|
|257  |HyperSearchOperationNode.java          |         1.03333|
|302  |JEditVisitor.java                      |         1.03333|
|504  |UndoManager.java                       |         1.03333|
|24   |BeanShellFacade.java                   |         1.00000|
|119  |ClockWidgetFactory.java                |         1.00000|
|163  |DockableWindowManagerProvider.java     |         1.00000|
|349  |MouseActions.java                      |         1.00000|
|431  |ScrollListener.java                    |         1.00000|
|95   |BufferSetAdapter.java                  |         0.96667|
|129  |ColumnBlockLine.java                   |         0.96667|
|381  |PingPongList.java                      |         0.96667|
|486  |TextAreaOptionPane.java                |         0.96667|
|167  |DockingOptionPane.java                 |         0.93333|
|218  |FileVFS.java                           |         0.93333|
|280  |JARClassLoader.java                    |         0.93333|
|482  |TextAreaException.java                 |         0.93333|
|497  |ToolBarManager.java                    |         0.93333|
|132  |CompleteWord.java                      |         0.90000|
|133  |CompletionPopup.java                   |         0.90000|
|470  |TabbedOptionDialog.java                |         0.90000|
|489  |TextUtilities.java                     |         0.90000|
|499  |ToolTipLabel.java                      |         0.90000|
|522  |ViewOptionPane.java                    |         0.90000|
|269  |InputHandlerProvider.java              |         0.86667|
|402  |PropertyManager.java                   |         0.86667|
|422  |RichJEditTextTransferableService.java  |         0.86667|
|462  |StringModel.java                       |         0.86667|
|87   |BufferOptionPane.java                  |         0.83333|
|115  |ClassPathException.java                |         0.83333|
|156  |DockableLayout.java                    |         0.83333|
|159  |DockableWindowContainer.java           |         0.83333|
|252  |HistoryTextField.java                  |         0.83333|
|416  |RegistersListener.java                 |         0.83333|
|454  |SquareFoldPainter.java                 |         0.83333|
|502  |Type.java                              |         0.83333|
|525  |Widget.java                            |         0.83333|
|272  |IntegerArray.java                      |         0.80000|
|300  |JEditTransferableService.java          |         0.80000|
|311  |Keymap.java                            |         0.80000|
|316  |KeymapMigration.java                   |         0.80000|
|328  |Log.java                               |         0.80000|
|430  |ScrollLineCount.java                   |         0.80000|
|444  |ServiceListHandler.java                |         0.80000|
|107  |Chunk.java                             |         0.76667|
|162  |DockableWindowManagerImpl.java         |         0.76667|
|181  |EditorExiting.java                     |         0.76667|
|264  |IndentFoldHandler.java                 |         0.76667|
|301  |JEditTrayIcon.java                     |         0.76667|
|441  |SelectionLengthWidgetFactory.java      |         0.76667|
|11   |ActionListHandler.java                 |         0.73333|
|27   |BracketIndentRule.java                 |         0.73333|
|80   |BufferHandler.java                     |         0.73333|
|100  |BufferUndoListener.java                |         0.73333|
|190  |EmacsUtil.java                         |         0.73333|
|297  |JEditSwingTrayIcon.java                |         0.73333|
|299  |JEditTransferable.java                 |         0.73333|
|413  |RegisterChanged.java                   |         0.73333|
|500  |TransferHandler.java                   |         0.73333|
|509  |VariableGridLayout.java                |         0.73333|
|136  |ContentManager.java                    |         0.70000|
|225  |FoldHandlerProvider.java               |         0.70000|
|287  |JEditBeanShellAction.java              |         0.70000|
|289  |JEditDataFlavor.java                   |         0.70000|
|428  |ScreenLineManager.java                 |         0.70000|
|483  |TextAreaExtension.java                 |         0.70000|
|188  |ElasticTabStopBufferListener.java      |         0.68966|
|131  |CommandLineReader.java                 |         0.66667|
|234  |GrabKeyDialog.java                     |         0.66667|
|391  |PluginResURLConnection.java            |         0.66667|
|101  |BufferUpdate.java                      |         0.63333|
|120  |CloseBracketIndentRule.java            |         0.63333|
|154  |DisplayManager.java                    |         0.63333|
|164  |DockableWindowUpdate.java              |         0.63333|
|173  |EBComponent.java                       |         0.63333|
|209  |ExtensionManager.java                  |         0.63333|
|230  |GeneralOptionPane.java                 |         0.63333|
|270  |InputMethodSupport.java                |         0.63333|
|295  |JEditRegistersListener.java            |         0.63333|
|306  |JTrayIcon.java                         |         0.63333|
|475  |TaskManager.java                       |         0.63333|
|44   |BshClassLoader.java                    |         0.60000|
|77   |BufferAdapter.java                     |         0.60000|
|142  |DeepIndentRule.java                    |         0.60000|
|247  |HistoryButton.java                     |         0.60000|
|323  |LineSepWidgetFactory.java              |         0.60000|
|362  |OpenBracketIndentRule.java             |         0.60000|
|429  |ScrollLayout.java                      |         0.60000|
|476  |TaskMonitor.java                       |         0.60000|
|105  |CharsetEncoding.java                   |         0.56667|
|123  |CodeWriter.java                        |         0.56667|
|227  |FoldWidgetFactory.java                 |         0.56667|
|256  |HyperSearchNode.java                   |         0.56667|
|386  |PluginManager.java                     |         0.56667|
|529  |XModeHandler.java                      |         0.56667|
|193  |EncodingServer.java                    |         0.53333|
|290  |JEditEmbeddedTextArea.java             |         0.53333|
|351  |MouseHandler.java                      |         0.53333|
|459  |StatusListener.java                    |         0.53333|
|90   |BufferPrinter1_3.java                  |         0.50000|
|144  |DefaultFoldHandlerProvider.java        |         0.50000|
|196  |EncodingWithBOM.java                   |         0.50000|
|284  |JEditAbstractEditAction.java           |         0.50000|
|415  |RegisterSaver.java                     |         0.50000|
|438  |SegmentBuffer.java                     |         0.50000|
|102  |ByteVector.java                        |         0.46667|
|231  |GlobalOptionGroup.java                 |         0.46667|
|293  |JEditMode.java                         |         0.46667|
|318  |KillRing.java                          |         0.46667|
|501  |TriangleFoldPainter.java               |         0.46667|
|17   |AppearanceOptionPane.java              |         0.43333|
|201  |EnhancedMenuItem.java                  |         0.43333|
|356  |NameSource.java                        |         0.43333|
|485  |TextAreaMouseHandler.java              |         0.43333|
|204  |ErrorsWidgetFactory.java               |         0.40000|
|403  |RangeMap.java                          |         0.40000|
|75   |BSHWhileStatement.java                 |         0.36667|
|189  |ElasticTabstopsTabExpander.java        |         0.36667|
|203  |ErrorListDialog.java                   |         0.36667|
|370  |ParseException.java                    |         0.36667|
|427  |SaveCaretInfoVisitor.java              |         0.36667|
|192  |EncodingDetector.java                  |         0.33333|
|220  |FilteredTableModel.java                |         0.33333|
|236  |Gutter.java                            |         0.33333|
|481  |TextAreaDropHandler.java               |         0.33333|
|93   |BufferSegment.java                     |         0.30000|
|377  |PasteFromListDialog.java               |         0.30000|
|423  |RichTextTransferable.java              |         0.30000|
|360  |NumericTextField.java                  |         0.27586|
|365  |OptionGroupPane.java                   |         0.26667|
|399  |PropertiesBean.java                    |         0.26667|
|527  |XMLEncodingDetector.java               |         0.26667|
|168  |DummyFoldHandler.java                  |         0.23333|
|169  |DummyTokenHandler.java                 |         0.23333|
|395  |PositionManager.java                   |         0.23333|
|412  |RegexpIndentRule.java                  |         0.23333|
|466  |StyleEditor.java                       |         0.23333|
|439  |SegmentCharSequence.java               |         0.20000|
|524  |WhitespaceRule.java                    |         0.20000|
|108  |ChunkCache.java                        |         0.16667|
|238  |GutterPopupHandler.java                |         0.16667|
|262  |IconTheme.java                         |         0.16667|
|448  |ShapedFoldPainter.java                 |         0.16667|
|266  |IndentRuleFactory.java                 |         0.13333|
|254  |HyperSearchFileNode.java               |         0.10000|
|265  |IndentRule.java                        |         0.10000|
|390  |PluginOptions.java                     |         0.06667|
|436  |SearchMatcher.java                     |         0.03448|
|109  |CircleFoldPainter.java                 |         0.00000|
|342  |MiscUtilities.java                     |         0.00000|
|461  |StringList.java                        |         0.00000|
|495  |TokenMarker.java                       |        -0.56667|
|317  |KeywordMap.java                        |        -8.46667|
|147  |DefaultTokenHandler.java               |       -13.16667|

