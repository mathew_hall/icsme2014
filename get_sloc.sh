#!/usr/bin/env bash


#Assume that we're already in the right directory.

if [ ! -n "$1" ]; then
	echo "Need to supply a path to a FOLDER of sil files"
	exit
fi

SIL_DIR="$1"

echo "" > /tmp/sil_file_list.txt

#collect all names in SILs
for f in $SIL_DIR/*.sil; do
	cut -d "=" -f 2 < $f | tr "," "\n" | tr -d " " | tr -d "\r" | tr -d '"' | tr "." "/"  > /tmp/sil_file_list.txt
done

#get unique list:
sort <  /tmp/sil_file_list.txt | uniq > /tmp/class_list.txt

while read f; do 
	if [ -f "$f.java" ]; then 
		wc -l "$f.java";
	else
		 echo "$f NOT FOUND" >&2 
	fi; 
done < /tmp/class_list.txt