#!/usr/bin/env bash

if [ "$1" == "" ]; then
	echo "usage: $0 absolute_sil_path"
	exit -1
fi




export RUN_SIL=true
export SIL_FILE="$1"

if [ ! -f "eclipse" ]; then
	echo "Must be run from eclipse directory"
	exit -1
fi

export DISPLAY=:0.0


if [ ! -f "$SIL_FILE" ]; then
	echo "SIL file must exist, $SIL_FILE doesn't."
	exit -1
fi 

if [ -e "/tmp/sumopipe" ]; then
	
	if [ ! -p "/tmp/sumopipe" ]; then
		echo "removing old pipe"
		rm /tmp/sumopipe
		mkfifo /tmp/sumopipe
	fi
else
	echo "Creating IPC pipe"
	mkfifo /tmp/sumopipe
fi
if [ -n "$WORKSPACE"  ]; then
        ./eclipse -data "$WORKSPACE" &
else
        ./eclipse &
fi

echo "Waiting a bit for eclipse to start..."
sleep 30

if [ ! -n "$WORKSPACE"  ]; then
        echo "Assumed eclipse is open. Hitting OK on workspace selector"
        xdotool mousemove 757 477
        xdotool click 1
        
        echo "Waiting for workspace to load"
        sleep 20
fi



echo "Hitting refresh"
xdotool key F5

echo "Waiting for repo to refresh"

sleep 60

xdotool mousemove 18 183
xdotool click 1
sleep 1



echo "Opening context menu on project"
xdotool mousemove 53 214
xdotool click 3

sleep 1

echo "Starting SUMO"
xdotool mousemove 137 691
xdotool click 1

echo "Waiting for SUMO to start and settle"
sleep 10

echo "Clicking Refactor button"
xdotool mousemove 335 124
xdotool click 1

echo "Waiting for refactoring to finish (might take a while)"
RESULT=$(cat /tmp/sumopipe)
if [ "$RESULT" == "SUCCESS" ]; then
	echo ">>> Refactoring finished successfully"
else
	echo ">>> Refactoring failed to complete"
fi

echo "Refactoring completed"
echo "Closing SUMO"
xdotool key --clearmodifiers alt+F4

echo "Waiting for workspace to refresh"
sleep 60

scrot "$SIL_FILE.png"


echo "Exiting Eclipse"
xdotool mousemove 1020 36
xdotool click 1