#!/usr/bin/env perl
use File::Basename;
use Data::Dumper;

my $sil_file=$ARGV[0];
my $svn_root=$ARGV[1];
my $wkp_root=$ARGV[2];
my $run = $ARGV[3] or 0;
my $svn = $ARGV[4] or $run;


open(SIL, '<', $sil_file) or die("Usage: $0 path_to_sil path_to_svn path_to_workspace");

my %workspace_to_svn;
my %svn_to_workspace;

sub fqname_to_class{
	my $class = shift;
	my @split_name = split(/\./, $class);
	return $split_name[$#split_name];
}

sub fqname_to_path{
	my $class = shift;
	$class =~ s|\.|/|g;
	return $class;
}

my @packages;

while(<SIL>){
	s/"//g;
	s/\r//g;
	chomp;
	my ($package,$classlist) = split(/=/);
	
	$package =~ s/SS\(//;
	$package =~ s/\)//;
	
	my @classes = split(/,\s*/, $classlist);

	my @current;
	
        push(@packages,$package);
        
	#TODO:
	# populate mapping of workspace -> svn
	# and svn -> workspace.
	foreach my $class (@classes){
		my $current_name = $package. "/". fqname_to_class($class) . ".java";

		my $original_name = fqname_to_path($class).".java";

		$workspace_to_svn{$current_name} = $original_name;
		$svn_to_workspace{$original_name} = $current_name;
	}
	

}

close(SIL);

foreach my $package (@packages){
        my $svn_command = "svn add $svn_root/$package";
        if($svn){
                `$svn_command`
        }else{
                print $svn_command,"\n";
        }
}

foreach my $file_in_workspace (keys %workspace_to_svn){
	
	$current_filename = "$wkp_root/$file_in_workspace";
	$original_filename = "$svn_root/$workspace_to_svn{$file_in_workspace}";
	$desired_filename = "$svn_root/$file_in_workspace";
	
	my $move_to_svn = "mv $current_filename $original_filename";
	
	my $svn_rename = "svn move $original_filename $desired_filename";
	
        
	if($run){
                #Move the file back to its original place in the repo
		`$move_to_svn`;
        }else{
		print $move_to_svn."\n";
        }
        
}

foreach my $file_in_workspace (keys %workspace_to_svn){
	
	$current_filename = "$wkp_root/$file_in_workspace";
	$original_filename = "$svn_root/$workspace_to_svn{$file_in_workspace}";
	$desired_filename = "$svn_root/$file_in_workspace";
	
	my $move_to_svn = "mv $current_filename $original_filename";
	
	my $svn_rename = "svn move $original_filename $desired_filename";
	

	
        #Are we issuing SVN commands? (cmd line argument)
        if($svn){
                #Where will this file go?
                #If the package folder doesn't exist (it should), create it
                #and add it to SVN:
		$package_dir = dirname($desired_filename);
		if(! -d $package_dir){
			print "Making $package_dir\n";
			mkdir $package_dir;
			`svn add $package_dir`
		}
                
                #Tell SVN to move the file from its original location to its new location,
                #copying its history with it
		`$svn_rename`;
                #SVN move issues two commits: Add (with copied history) and Delete.
                #The delete balloons the commit size, so we revert the file to remove
                #it from the diff.
                #Note: this was a choice for our experiment.
		`svn revert $original_filename`;
        }else{
		print $svn_rename."\n\n";
	}

}