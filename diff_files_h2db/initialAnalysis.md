# SUMO Commit sizes for H2DB


```r

args <- commandArgs(TRUE)

args
```

```
## [1] "WOLOLO"
```

```r

require(ggplot2)
```

```
## Loading required package: ggplot2
## Loading required package: methods
```

```r
require(plyr)
```

```
## Loading required package: plyr
```

```r

sizes <- read.csv("sizes_filter.csv", header = F, stringsAsFactors = F)

names(sizes) <- c("sil_file", "file", "original_hunk_size", "new_hunk_size", 
    "additions", "deletions")



package <- function(string) {
    sapply(strsplit(string, "/"), function(x) x[1])
}

class <- function(string) {
    sapply(strsplit(string, "/"), function(x) x[2])
}

sizes$package <- package(sizes$file)
sizes$class <- class(sizes$file)


# Sanity checks: make sure that the sum of additions and deletions is
# consistent with hunk sizes:
netChanges <- sizes$additions - sizes$deletions
hunkChanges <- sizes$new_hunk_size - sizes$original_hunk_size

differences <- any((netChanges - hunkChanges) != 0)

# any differences?
differences
```

```
## [1] TRUE
```

```r

if (differences) {
    print("Error: some net changes don't correspond to hunk size changes")
}
```

```
## [1] "Error: some net changes don't correspond to hunk size changes"
```


## Distributions of additions and deletions for each commit hunk

A diff is divided into "hunks", contiguous changes in a file. A file might have more than one hunk.
This histogram shows the distribution of additions and deletions in each hunk in green and red respectively.

Each separate plot represents a single Bunch result.


```r
d_ply(sizes, .(sil_file), function(df) {
    sil_file <- df$sil_file[1]
    # ggplot(df, aes(file,additions+deletions, fill=file)) +
    # geom_bar(stat='identity');
    
    print(ggplot(df, aes(additions)) + geom_histogram(fill = "green", alpha = 0.4, 
        binwidth = 5) + geom_histogram(aes(deletions), fill = "red", alpha = 0.4, 
        binwidth = 5) + labs(x = "Lines") + theme_minimal())
    # ggsave(paste0(sil_file,'.png'));
})
```

![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-21.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-22.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-23.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-24.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-25.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-26.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-27.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-28.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-29.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-210.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-211.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-212.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-213.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-214.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-215.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-216.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-217.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-218.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-219.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-220.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-221.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-222.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-223.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-224.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-225.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-226.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-227.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-228.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-229.png) ![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-230.png) 


Summary of commit details for each Bunch run


```r
total_sizes <- ddply(sizes, .(sil_file), summarise, total_additions = sum(additions), 
    total_deletions = sum(deletions))
```


|sil_file  |  total_additions|  total_deletions|
|:---------|----------------:|----------------:|
|0.sil     |             4303|             3937|
|1.sil     |             4415|             3927|
|10.sil    |             4362|             3937|
|11.sil    |             4399|             3918|
|12.sil    |             4319|             3938|
|13.sil    |             4376|             3928|
|14.sil    |             4355|             3930|
|15.sil    |             4396|             3937|
|16.sil    |             4395|             3932|
|17.sil    |             4417|             3937|
|18.sil    |             4274|             3939|
|19.sil    |             4376|             3939|
|2.sil     |             4351|             3920|
|20.sil    |             4363|             3936|
|21.sil    |             4421|             3936|
|22.sil    |             4417|             3938|
|23.sil    |             4379|             3927|
|24.sil    |             4350|             3936|
|25.sil    |             4443|             3939|
|26.sil    |             4363|             3923|
|27.sil    |             4348|             3939|
|28.sil    |             4316|             3935|
|29.sil    |             4374|             3929|
|3.sil     |             4387|             3938|
|4.sil     |             4365|             3935|
|5.sil     |             4410|             3929|
|6.sil     |             4419|             3939|
|7.sil     |             4410|             3939|
|8.sil     |             4370|             3936|
|9.sil     |             4399|             3937|


## Distribution of total lines added/deleted in total


```r
ggplot(total_sizes, aes(total_additions)) + geom_histogram(fill = "green", alpha = 0.4, 
    binwidth = 50) + geom_histogram(aes(total_deletions), fill = "red", alpha = 0.4, 
    binwidth = 50) + labs(x = "Lines") + theme_minimal()
```

![plot of chunk unnamed-chunk-5](figure/unnamed-chunk-5.png) 


It's quite clear that there are more additions than deletions, but the spread for deletions is much tighter. If we look at each point individually:


```r

ggplot(total_sizes, aes(total_additions, total_deletions)) + geom_point() + 
    labs(x = "Lines added", y = "Lines removed") + theme_minimal()
```

![plot of chunk unnamed-chunk-6](figure/unnamed-chunk-6.png) 


There are clear groups. The largest separation is between two populations with different numbers of lines removed and another with lines added.

This could correspond to different solutions Bunch produced, with the large separation occurring between two clearly different results. The smaller, within-group variation could be slight changes made by Bunch, i.e. points clustered around a local optimum.

## Averages:


```r
summary(total_sizes)
```

```
##    sil_file         total_additions total_deletions
##  Length:30          Min.   :4274    Min.   :3918   
##  Class :character   1st Qu.:4357    1st Qu.:3929   
##  Mode  :character   Median :4376    Median :3936   
##                     Mean   :4376    Mean   :3934   
##                     3rd Qu.:4407    3rd Qu.:3938   
##                     Max.   :4443    Max.   :3939
```


## How many hunks were only edits?


```r
hunkChanges <- ddply(sizes, .(sil_file), function(df) {
    df$changes <- df$additions - df$deletions
    df$netZero <- "NoChange"
    df$netZero[df$changes < 0] <- "Decrease"
    df$netZero[df$changes > 0] <- "Inrease"
    
    # print(ggplot(df, aes(changes)) + geom_histogram() + theme_minimal())
    print(ggplot(df, aes(netZero)) + geom_histogram() + theme_minimal() + labs(x = "Hunk did not change number of lines"))
    return(df)
})
```

![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-81.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-82.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-83.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-84.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-85.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-86.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-87.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-88.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-89.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-810.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-811.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-812.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-813.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-814.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-815.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-816.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-817.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-818.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-819.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-820.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-821.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-822.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-823.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-824.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-825.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-826.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-827.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-828.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-829.png) ![plot of chunk unnamed-chunk-8](figure/unnamed-chunk-830.png) 

```r

```


## Are net additions localised to certain files?


```r
largestChanges <- ddply(sizes, .(sil_file), function(sil) {
    perFile <- ddply(sil, .(sil_file, class), summarise, total_change = sum(additions - 
        deletions))
    perFile <- perFile[order(-perFile$total_change), ]
    perFile
})

averages <- ddply(largestChanges, .(class), summarise, average_change = mean(total_change))
averages <- averages[order(-averages$average_change), ]
kable(averages)
```

|id   |class                             |  average_change|
|:----|:---------------------------------|---------------:|
|292  |Parser.java                       |         5.53333|
|111  |DateTimeUtils.java                |         5.13333|
|116  |DbException.java                  |         4.10000|
|429  |Value.java                        |         3.76667|
|426  |UserAggregate.java                |         3.50000|
|398  |TableLinkConnection.java          |         3.23333|
|348  |SelectOrderBy.java                |         3.16667|
|104  |DatabaseEventListener.java        |         3.03333|
|193  |IndexInfo.java                    |         3.00000|
|187  |HashBase.java                     |         2.96429|
|448  |ValueResultSet.java               |         2.83333|
|99   |Cursor.java                       |         2.66667|
|335  |ScanIndex.java                    |         2.66667|
|144  |Engine.java                       |         2.60000|
|375  |SmallLRUCache.java                |         2.60000|
|2    |AggregateAPI.java                 |         2.50000|
|277  |PageDataOverflow.java             |         2.50000|
|63   |Compressor.java                   |         2.46667|
|333  |RunScriptCommand.java             |         2.46667|
|238  |MetaRecord.java                   |         2.43333|
|76   |Constants.java                    |         2.33333|
|236  |MetaCursor.java                   |         2.26667|
|38   |CacheSecondLevel.java             |         2.23333|
|70   |ConditionInSelect.java            |         2.23333|
|443  |ValueJavaObject.java              |         2.16667|
|257  |New.java                          |         2.13333|
|388  |StringUtils.java                  |         2.13333|
|394  |TableEngine.java                  |         2.13333|
|422  |UndoLogRecord.java                |         2.13333|
|417  |Trigger.java                      |         2.10000|
|365  |SetTypes.java                     |         2.03333|
|382  |SpatialIndex.java                 |         2.03333|
|83   |CreateAggregate.java              |         2.00000|
|201  |JavaFunction.java                 |         2.00000|
|228  |LobStorageInterface.java          |         2.00000|
|352  |SequenceValue.java                |         2.00000|
|243  |MultiVersionIndex.java            |         1.96667|
|357  |SessionInterface.java             |         1.96667|
|442  |ValueInt.java                     |         1.96667|
|50   |CommandContainer.java             |         1.90000|
|347  |SelectListColumnResolver.java     |         1.90000|
|418  |TriggerAdapter.java               |         1.90000|
|4    |AggregateDataCount.java           |         1.86667|
|232  |LZFInputStream.java               |         1.86667|
|299  |PrepareProcedure.java             |         1.83333|
|324  |Rownum.java                       |         1.83333|
|441  |ValueHashMap.java                 |         1.80000|
|319  |Right.java                        |         1.79310|
|456  |Variable.java                     |         1.76667|
|139  |DropTable.java                    |         1.75862|
|254  |MVTable.java                      |         1.73333|
|204  |JdbcBatchUpdateException.java     |         1.70000|
|249  |MVRTreeMap.java                   |         1.70000|
|79   |ConstraintReferential.java        |         1.66667|
|314  |ResultExternal.java               |         1.66667|
|25   |AutoCloseInputStream.java         |         1.63333|
|404  |ToChar.java                       |         1.63333|
|440  |ValueGeometry.java                |         1.63333|
|66   |ConditionAndOr.java               |         1.60000|
|86   |CreateFunctionAlias.java          |         1.60000|
|109  |DataType.java                     |         1.60000|
|246  |MVMap.java                        |         1.60000|
|396  |TableFunction.java                |         1.60000|
|447  |ValueNull.java                    |         1.60000|
|452  |ValueStringIgnoreCase.java        |         1.60000|
|399  |TableView.java                    |         1.58621|
|23   |AlterView.java                    |         1.56667|
|189  |Index.java                        |         1.56667|
|215  |JdbcResultSet.java                |         1.56667|
|297  |PlanItem.java                     |         1.56667|
|379  |SortOrder.java                    |         1.55172|
|85   |CreateConstant.java               |         1.53333|
|227  |LobStorageFrontend.java           |         1.53333|
|322  |Row.java                          |         1.53333|
|337  |SchemaCommand.java                |         1.53333|
|431  |ValueBoolean.java                 |         1.53333|
|457  |ViewCursor.java                   |         1.53333|
|363  |Setting.java                      |         1.51724|
|12   |Alias.java                        |         1.50000|
|192  |IndexCursor.java                  |         1.50000|
|150  |ExpressionList.java               |         1.48276|
|143  |DropView.java                     |         1.46667|
|191  |IndexCondition.java               |         1.46667|
|225  |LinkSchema.java                   |         1.46667|
|270  |PageBtreeLeaf.java                |         1.46667|
|276  |PageDataNode.java                 |         1.46667|
|27   |BackupCommand.java                |         1.43333|
|149  |ExpressionColumn.java             |         1.43333|
|255  |MVTableEngine.java                |         1.43333|
|315  |ResultInterface.java              |         1.43333|
|370  |SimpleRow.java                    |         1.43333|
|69   |ConditionInConstantSet.java       |         1.40000|
|107  |DataHandler.java                  |         1.40000|
|188  |HashIndex.java                    |         1.40000|
|220  |JdbcUtils.java                    |         1.40000|
|251  |MVSpatialIndex.java               |         1.40000|
|421  |UndoLog.java                      |         1.40000|
|29   |BitField.java                     |         1.36667|
|34   |CacheHead.java                    |         1.36667|
|167  |FilePathWrapper.java              |         1.36667|
|275  |PageDataLeaf.java                 |         1.36667|
|121  |DbSettings.java                   |         1.33333|
|183  |FunctionIndex.java                |         1.33333|
|185  |FunctionTable.java                |         1.33333|
|362  |SetComment.java                   |         1.33333|
|377  |SoftHashMap.java                  |         1.33333|
|386  |StreamStore.java                  |         1.33333|
|401  |TcpServer.java                    |         1.33333|
|433  |ValueBytes.java                   |         1.31034|
|55   |CompareLike.java                  |         1.30000|
|134  |DropFunctionAlias.java            |         1.30000|
|140  |DropTrigger.java                  |         1.30000|
|223  |LinkedCursor.java                 |         1.30000|
|242  |MultiVersionCursor.java           |         1.30000|
|258  |NonUniqueHashCursor.java          |         1.30000|
|306  |RangeTable.java                   |         1.30000|
|308  |Recover.java                      |         1.30000|
|392  |Table.java                        |         1.30000|
|30   |BlockCipher.java                  |         1.26667|
|57   |CompareModeDefault.java           |         1.26667|
|237  |MetaIndex.java                    |         1.26667|
|259  |NonUniqueHashIndex.java           |         1.26667|
|266  |Page.java                         |         1.26667|
|361  |Set.java                          |         1.26667|
|397  |TableLink.java                    |         1.26667|
|438  |ValueExpression.java              |         1.26667|
|445  |ValueLobDb.java                   |         1.26667|
|455  |ValueUuid.java                    |         1.26667|
|148  |Expression.java                   |         1.24138|
|95   |CreateUser.java                   |         1.23333|
|184  |FunctionInfo.java                 |         1.23333|
|263  |Operation.java                    |         1.23333|
|290  |ParameterInterface.java           |         1.23333|
|100  |CursorPos.java                    |         1.20690|
|7    |AggregateDataGroupConcat.java     |         1.20000|
|450  |ValueString.java                  |         1.20000|
|464  |Wildcard.java                     |         1.20000|
|68   |ConditionIn.java                  |         1.16667|
|78   |ConstraintCheck.java              |         1.16667|
|88   |CreateLinkedTable.java            |         1.16667|
|244  |MVDelegateIndex.java              |         1.16667|
|416  |TreeNode.java                     |         1.16667|
|434  |ValueDataType.java                |         1.16667|
|390  |SynchronizedVerifier.java         |         1.13793|
|19   |AlterTableRename.java             |         1.13333|
|40   |CacheWriter.java                  |         1.13333|
|105  |DatabaseInfo.java                 |         1.13333|
|213  |JdbcParameterMetaData.java        |         1.13333|
|289  |Parameter.java                    |         1.13333|
|372  |SimpleRowValue.java               |         1.13333|
|374  |SingleRowCursor.java              |         1.13333|
|411  |TransactionCommand.java           |         1.13333|
|432  |ValueByte.java                    |         1.13333|
|87   |CreateIndex.java                  |         1.10345|
|344  |SearchRow.java                    |         1.10345|
|9    |AggregateDataSelectivity.java     |         1.10000|
|48   |ColumnResolver.java               |         1.10000|
|59   |Comparison.java                   |         1.10000|
|115  |DbDriverActivator.java            |         1.10000|
|132  |DropConstant.java                 |         1.10000|
|135  |DropIndex.java                    |         1.10000|
|245  |MVIndex.java                      |         1.10000|
|267  |PageBtree.java                    |         1.10000|
|285  |PageStore.java                    |         1.10000|
|342  |ScriptCommand.java                |         1.10000|
|415  |TreeIndex.java                    |         1.10000|
|93   |CreateTableData.java              |         1.06667|
|141  |DropUser.java                     |         1.06667|
|197  |IntArray.java                     |         1.06667|
|241  |MultiDimension.java               |         1.06667|
|286  |PageStoreInDoubtTransaction.java  |         1.06667|
|303  |QueryStatisticsData.java          |         1.06667|
|334  |ScanCursor.java                   |         1.06667|
|414  |TreeCursor.java                   |         1.06667|
|436  |ValueDecimal.java                 |         1.06667|
|13   |AlterIndexRename.java             |         1.03333|
|15   |AlterSequence.java                |         1.03333|
|26   |Backup.java                       |         1.03333|
|46   |CloseWatcher.java                 |         1.03333|
|194  |IndexType.java                    |         1.03333|
|346  |Select.java                       |         1.03333|
|460  |WebServer.java                    |         1.03333|
|14   |AlterSchemaRename.java            |         1.00000|
|24   |Analyze.java                      |         1.00000|
|28   |BaseIndex.java                    |         1.00000|
|65   |Condition.java                    |         1.00000|
|71   |ConditionNot.java                 |         1.00000|
|172  |FileUtils.java                    |         1.00000|
|234  |MathUtils.java                    |         1.00000|
|271  |PageBtreeNode.java                |         1.00000|
|300  |Procedure.java                    |         1.00000|
|381  |SpatialDataType.java              |         1.00000|
|60   |CompressDeflate.java              |         0.96667|
|67   |ConditionExists.java              |         0.96667|
|190  |IndexColumn.java                  |         0.96667|
|205  |JdbcBlob.java                     |         0.96667|
|280  |PageIndex.java                    |         0.96667|
|282  |PageLog.java                      |         0.96667|
|287  |PageStreamData.java               |         0.96667|
|304  |RangeCursor.java                  |         0.96667|
|341  |ScriptBase.java                   |         0.96667|
|389  |Subquery.java                     |         0.96667|
|33   |Cache.java                        |         0.93333|
|39   |CacheTQ.java                      |         0.93333|
|49   |Command.java                      |         0.93333|
|94   |CreateTrigger.java                |         0.93333|
|126  |DefineCommand.java                |         0.93333|
|127  |Delete.java                       |         0.93333|
|179  |FunctionAlias.java                |         0.93333|
|180  |FunctionCall.java                 |         0.93333|
|231  |LocalResult.java                  |         0.93333|
|305  |RangeIndex.java                   |         0.93333|
|310  |RegularTable.java                 |         0.93333|
|351  |Sequence.java                     |         0.93333|
|384  |SpatialTreeIndex.java             |         0.93333|
|385  |StatementBuilder.java             |         0.93333|
|412  |TransactionStore.java             |         0.93333|
|413  |Transfer.java                     |         0.93333|
|424  |Update.java                       |         0.93333|
|428  |Utils.java                        |         0.93333|
|453  |ValueTime.java                    |         0.93333|
|131  |DropAggregate.java                |         0.93103|
|58   |CompareModeIcu4J.java             |         0.90000|
|72   |ConnectionInfoEngine.java         |         0.90000|
|82   |CountingReaderInputStream.java    |         0.90000|
|97   |CreateView.java                   |         0.90000|
|293  |Permutations.java                 |         0.90000|
|51   |CommandInterface.java             |         0.89655|
|129  |DocumentedMBean.java              |         0.89655|
|8    |AggregateDataHistogram.java       |         0.86667|
|43   |ChangeFileEncryption.java         |         0.86667|
|54   |Comment.java                      |         0.86667|
|169  |FileStore.java                    |         0.86667|
|178  |Function.java                     |         0.86667|
|195  |InDoubtTransaction.java           |         0.86667|
|240  |Mode.java                         |         0.86667|
|281  |PageInputStream.java              |         0.86667|
|283  |PageOutputStream.java             |         0.86667|
|295  |PgServerThread.java               |         0.86667|
|427  |UserDataType.java                 |         0.86667|
|5    |AggregateDataCountAll.java        |         0.83333|
|181  |FunctionCursor.java               |         0.83333|
|298  |Prepared.java                     |         0.83333|
|373  |SingleColumnResolver.java         |         0.83333|
|376  |SmallMap.java                     |         0.83333|
|402  |TcpServerThread.java              |         0.83333|
|403  |TempFileDeleter.java              |         0.83333|
|409  |TraceWriter.java                  |         0.83333|
|466  |WriterThread.java                 |         0.83333|
|47   |Column.java                       |         0.80000|
|108  |DataReader.java                   |         0.80000|
|142  |DropUserDataType.java             |         0.80000|
|151  |ExpressionVisitor.java            |         0.80000|
|217  |JdbcSavepoint.java                |         0.80000|
|291  |ParameterRemote.java              |         0.80000|
|354  |Service.java                      |         0.80000|
|371  |SimpleRowSource.java              |         0.80000|
|393  |TableBase.java                    |         0.80000|
|454  |ValueTimestamp.java               |         0.80000|
|458  |ViewIndex.java                    |         0.80000|
|133  |DropDatabase.java                 |         0.76667|
|200  |JavaAggregate.java                |         0.76667|
|224  |LinkedIndex.java                  |         0.76667|
|239  |MetaTable.java                    |         0.76667|
|250  |MVSecondaryIndex.java             |         0.76667|
|273  |PageDataCursor.java               |         0.76667|
|323  |RowList.java                      |         0.76667|
|360  |SessionWithState.java             |         0.76667|
|467  |XTEA.java                         |         0.76667|
|117  |DbObject.java                     |         0.73333|
|157  |FilePath.java                     |         0.73333|
|196  |Insert.java                       |         0.73333|
|199  |IOUtils.java                      |         0.73333|
|264  |Optimizer.java                    |         0.73333|
|268  |PageBtreeCursor.java              |         0.73333|
|272  |PageData.java                     |         0.73333|
|278  |PageDelegateIndex.java            |         0.73333|
|336  |Schema.java                       |         0.73333|
|439  |ValueFloat.java                   |         0.73333|
|3    |AggregateData.java                |         0.70000|
|22   |AlterUser.java                    |         0.70000|
|52   |CommandList.java                  |         0.70000|
|61   |CompressLZF.java                  |         0.70000|
|165  |FilePathRec.java                  |         0.70000|
|182  |FunctionCursorResultSet.java      |         0.70000|
|235  |Merge.java                        |         0.70000|
|318  |ResultTempTable.java              |         0.70000|
|349  |SelectUnion.java                  |         0.70000|
|369  |SimpleResultSet.java              |         0.70000|
|395  |TableFilter.java                  |         0.70000|
|400  |Task.java                         |         0.70000|
|423  |UpdatableRow.java                 |         0.70000|
|425  |User.java                         |         0.70000|
|451  |ValueStringFixed.java             |         0.70000|
|11   |AggregateFunction.java            |         0.66667|
|53   |CommandRemote.java                |         0.66667|
|56   |CompareMode.java                  |         0.66667|
|198  |IntIntHashMap.java                |         0.66667|
|216  |JdbcResultSetMetaData.java        |         0.66667|
|229  |LobStorageMap.java                |         0.66667|
|274  |PageDataIndex.java                |         0.66667|
|311  |Replace.java                      |         0.66667|
|329  |RuleList.java                     |         0.66667|
|355  |Session.java                      |         0.66667|
|359  |SessionState.java                 |         0.66667|
|435  |ValueDate.java                    |         0.66667|
|446  |ValueLong.java                    |         0.66667|
|419  |TriggerObject.java                |         0.65517|
|17   |AlterTableAlterColumn.java        |         0.63333|
|41   |Call.java                         |         0.63333|
|91   |CreateSequence.java               |         0.63333|
|101  |Data.java                         |         0.63333|
|102  |Database.java                     |         0.63333|
|106  |DatabaseInfoMBean.java            |         0.63333|
|162  |FilePathNio.java                  |         0.63333|
|248  |MVPrimaryIndex.java               |         0.63333|
|256  |NetUtils.java                     |         0.63333|
|307  |Recorder.java                     |         0.63333|
|364  |SettingsBase.java                 |         0.63333|
|18   |AlterTableDropConstraint.java     |         0.62069|
|6    |AggregateDataDefault.java         |         0.60000|
|147  |Explain.java                      |         0.60000|
|159  |FilePathDisk.java                 |         0.60000|
|174  |FreeSpaceBitSet.java              |         0.60000|
|203  |JdbcArray.java                    |         0.60000|
|226  |LobStorageBackend.java            |         0.60000|
|230  |LobStorageRemoteInputStream.java  |         0.60000|
|302  |Query.java                        |         0.60000|
|320  |RightOwner.java                   |         0.60000|
|338  |SchemaObject.java                 |         0.60000|
|1    |AES.java                          |         0.56667|
|75   |Constant.java                     |         0.56667|
|155  |FileLister.java                   |         0.56667|
|339  |SchemaObjectBase.java             |         0.56667|
|356  |SessionFactory.java               |         0.56667|
|430  |ValueArray.java                   |         0.56667|
|444  |ValueLob.java                     |         0.56667|
|35   |CacheLongKeyLIRS.java             |         0.53333|
|45   |CipherFactory.java                |         0.53333|
|90   |CreateSchema.java                 |         0.53333|
|92   |CreateTable.java                  |         0.53333|
|166  |FilePathSplit.java                |         0.53333|
|171  |FileStoreOutputStream.java        |         0.53333|
|173  |Fog.java                          |         0.53333|
|202  |JavaObjectSerializer.java         |         0.53333|
|214  |JdbcPreparedStatement.java        |         0.53333|
|260  |NoOperation.java                  |         0.53333|
|279  |PageFreeList.java                 |         0.53333|
|343  |ScriptReader.java                 |         0.53333|
|391  |SysProperties.java                |         0.53333|
|10   |AggregateExpression.java          |         0.50000|
|110  |DataUtils.java                    |         0.50000|
|138  |DropSequence.java                 |         0.50000|
|146  |ExecuteProcedure.java             |         0.50000|
|175  |FullText.java                     |         0.50000|
|210  |JdbcDatabaseMetaData.java         |         0.50000|
|219  |JdbcStatement.java                |         0.50000|
|312  |Restore.java                      |         0.50000|
|345  |SecureFileStore.java              |         0.50000|
|366  |SHA256.java                       |         0.50000|
|367  |Shell.java                        |         0.50000|
|449  |ValueShort.java                   |         0.50000|
|177  |FullTextSettings.java             |         0.46667|
|207  |JdbcClob.java                     |         0.46667|
|261  |ObjectDataType.java               |         0.46667|
|301  |Profiler.java                     |         0.46667|
|309  |RecoverTester.java                |         0.46667|
|321  |Role.java                         |         0.46429|
|21   |AlterTableSet.java                |         0.44828|
|37   |CacheObject.java                  |         0.43333|
|89   |CreateRole.java                   |         0.43333|
|378  |SortedProperties.java             |         0.43333|
|420  |TruncateTable.java                |         0.43333|
|459  |WebApp.java                       |         0.43333|
|145  |ErrorCode.java                    |         0.41379|
|77   |Constraint.java                   |         0.40000|
|164  |FilePathNioMem.java               |         0.40000|
|316  |ResultRemote.java                 |         0.40000|
|406  |Trace.java                        |         0.40000|
|462  |WebSession.java                   |         0.40000|
|80   |ConstraintUnique.java             |         0.36667|
|81   |ConvertTraceFile.java             |         0.36667|
|98   |Csv.java                          |         0.36667|
|103  |DatabaseCloser.java               |         0.36667|
|208  |JdbcConnection.java               |         0.36667|
|156  |FileLock.java                     |         0.33333|
|170  |FileStoreInputStream.java         |         0.33333|
|206  |JdbcCallableStatement.java        |         0.33333|
|288  |PageStreamTrunk.java              |         0.33333|
|294  |PgServer.java                     |         0.33333|
|296  |Plan.java                         |         0.33333|
|317  |ResultTarget.java                 |         0.33333|
|353  |Server.java                       |         0.33333|
|358  |SessionRemote.java                |         0.33333|
|20   |AlterTableRenameColumn.java       |         0.30000|
|119  |DbProcedure.java                  |         0.30000|
|128  |DeleteDbFiles.java                |         0.30000|
|265  |OsgiDataSourceFactory.java        |         0.30000|
|437  |ValueDouble.java                  |         0.30000|
|262  |OffHeapStore.java                 |         0.26667|
|218  |JdbcSQLException.java             |         0.24138|
|42   |CaseInsensitiveMap.java           |         0.23333|
|74   |Console.java                      |         0.23333|
|137  |DropSchema.java                   |         0.23333|
|154  |FileChannelOutputStream.java      |         0.23333|
|160  |FilePathEncrypt.java              |         0.23333|
|186  |GrantRevoke.java                  |         0.23333|
|405  |Tool.java                         |         0.23333|
|16   |AlterTableAddConstraint.java      |         0.20000|
|62   |CompressNo.java                   |         0.20000|
|64   |CompressTool.java                 |         0.20000|
|96   |CreateUserDataType.java           |         0.20000|
|125  |DeallocateProcedure.java          |         0.20000|
|136  |DropRole.java                     |         0.20000|
|158  |FilePathCache.java                |         0.20000|
|161  |FilePathMem.java                  |         0.20000|
|252  |MVStore.java                      |         0.20000|
|269  |PageBtreeIndex.java               |         0.20000|
|368  |ShutdownHandler.java              |         0.20000|
|408  |TraceSystem.java                  |         0.20000|
|118  |DbObjectBase.java                 |         0.16667|
|168  |FilePathZip.java                  |         0.16667|
|209  |JdbcConnectionPool.java           |         0.16667|
|221  |JdbcXAConnection.java             |         0.16667|
|222  |JdbcXid.java                      |         0.16667|
|328  |RuleHead.java                     |         0.16667|
|380  |SourceCompiler.java               |         0.16667|
|120  |DbSchema.java                     |         0.13333|
|122  |DbStarter.java                    |         0.13333|
|163  |FilePathNioMapped.java            |         0.13333|
|326  |RuleElement.java                  |         0.13333|
|340  |Script.java                       |         0.13333|
|407  |TraceObject.java                  |         0.13333|
|31   |Bnf.java                          |         0.10000|
|32   |BnfVisitor.java                   |         0.10000|
|73   |ConnectionInfoWeb.java            |         0.10000|
|123  |DbTableOrView.java                |         0.10000|
|152  |FileBase.java                     |         0.10000|
|153  |FileChannelInputStream.java       |         0.10000|
|253  |MVStoreTool.java                  |         0.10000|
|332  |RunScript.java                    |         0.10000|
|383  |SpatialKey.java                   |         0.10000|
|387  |StringDataType.java               |         0.10000|
|410  |TraceWriterAdapter.java           |         0.10000|
|36   |CacheLRU.java                     |         0.06667|
|44   |Chunk.java                        |         0.06667|
|124  |DbUpgrade.java                    |         0.06667|
|130  |Driver.java                       |         0.06667|
|247  |MVMapConcurrent.java              |         0.06667|
|313  |ResultColumn.java                 |         0.06667|
|330  |RuleOptional.java                 |         0.06667|
|84   |CreateCluster.java                |         0.03333|
|114  |DbContextRule.java                |         0.03333|
|211  |JdbcDataSource.java               |         0.03333|
|212  |JdbcDataSourceFactory.java        |         0.03333|
|331  |RuleRepeat.java                   |         0.03333|
|465  |WriteBuffer.java                  |         0.03333|
|112  |DbColumn.java                     |         0.00000|
|113  |DbContents.java                   |         0.00000|
|233  |LZFOutputStream.java              |         0.00000|
|284  |PageParser.java                   |         0.00000|
|325  |Rule.java                         |         0.00000|
|327  |RuleFixed.java                    |         0.00000|
|350  |Sentence.java                     |         0.00000|
|461  |WebServlet.java                   |         0.00000|
|463  |WebThread.java                    |         0.00000|
|176  |FullTextLucene.java               |       -10.56667|

