#!/usr/bin/env perl

use strict;

my $silfile = $ARGV[0] or die ("Usage: $0 sil_name [filter_empty_lines]");
my $filter = $ARGV[1] or 0;


#We'll change these each time we see a new file, or
my $name = "";
#hunk:
my $originalSize = 0;
my $newSize = 0;

#Running counts:
my $additions = 0;
my $deletions = 0;

sub reset_counts(){
	$additions = 0;
	$deletions = 0;
}



sub log_hunk(){
	if($additions > 0 || $deletions > 0 || $originalSize > 0 || $newSize > 0){
		print "$silfile, $name, $originalSize, $newSize, $additions, $deletions\n";
		
	}
}

open(FILE, "<$silfile") or die ("Can't open $silfile for reading.");

while(<FILE>){
	#Start of file block
	if(/^Index: (\S+)$/){
		$name = $1;
	}
	
	#Start of hunk:
	if(/^@@ -\d+(,\d+)? \+\d+(,\d+)? @@$/){
		#GNU diff sometimes omits the hunk size if it's only one line.
		my $os = $1;
		my $as = $2;
		if(! $1){
			$os = 1;
		}else{
			$os  =~ tr/,//d;
		}
		if(! $2){
			$as = 1;
		}else{
			$as =~ tr/,//d;
		}
		log_hunk();
		reset_counts();
		$originalSize = $os;
		$newSize = $as;
		
	}
	
	if(/^(\+|-)$/){
		#ignore empty added/deleted lines.
                if($filter){
		        next;
                }
	}
	
	
	if(/\/\/(\}\}\}|\{\{\{)/){
		#ignore comment blocks Eclips sometimes edits.
                if($filter){
		        next;
                }
	}
	
	#line addition
	if(/^\+(?!\+\+)/){
		$additions++;
	}
	#line deletion
	if(/^-(?!--)/){
		$deletions++;
	}
	
}

close(FILE);